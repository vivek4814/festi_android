package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.muzified.R
import kotlinx.android.synthetic.main.toolbar_main.*

class ContactUsActivity: AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
        tv_toolbarTitle.text = ""
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
        }
    }
}