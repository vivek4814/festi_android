package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.firebase.firestore.FirebaseFirestore
import com.muzified.R
import com.muzified.Utils.*
import kotlinx.android.synthetic.main.activity_bio_seeking.*
import kotlinx.android.synthetic.main.toolbar_main.*

class BioSeekingActivity : AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null
    private var mGenderPreferences: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bio_seeking)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        tv_toolbarTitle.text = getString(R.string.title_bio_seeking)
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)

        lyt_male.setOnClickListener(this)
        lyt_female.setOnClickListener(this)
        lyt_both.setOnClickListener(this)

        btn_submit.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }

            R.id.lyt_male -> {
                updateGenderSelection(AppConstants.MALE)
            }
            R.id.lyt_female -> {
                updateGenderSelection(AppConstants.FEMALE)
            }
            R.id.lyt_both -> {
                updateGenderSelection(AppConstants.BOTH)
            }
            R.id.btn_submit -> {
                Utility.hideKeyboard(this, btn_submit)
                if (NetworkUtils.isNetworkAvailable(this@BioSeekingActivity)) {
                    if (isValidate()) {
                        submitBioSeeking()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (TextUtils.isEmpty(et_description.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_bio))
        } else if (TextUtils.isEmpty(mGenderPreferences)) {
            isValid = false
            ToastMsgUtils.showErrorMsg(
                lyt_parent,
                getString(R.string.error_msg_empty_gender_preferences)
            )
        } else {
            isValid = true
        }
        return isValid
    }

    private fun submitBioSeeking() {
        lyt_bar.visibility = View.VISIBLE
        val updateBio: MutableMap<String, Any> = HashMap()
        updateBio["bio"] = et_description.text.toString().trim()
        updateBio["gender_preference"] = mGenderPreferences
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(Prefs.getString(AppConstants.USER_ID))
            .update(updateBio)
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE
                goToNextActivity()
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(
                    lyt_parent,
                    "" + it.message
                )
            }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@BioSeekingActivity, GalleryPhotoActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private fun updateGenderSelection(genderType: String) {

        lyt_male.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_light_black)
        iv_male.setImageResource(R.mipmap.gender_unselected)

        lyt_female.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_light_black)
        iv_female.setImageResource(R.mipmap.gender_unselected)

        lyt_both.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_light_black)
        iv_both.setImageResource(R.mipmap.gender_unselected)

        when (genderType) {
            AppConstants.MALE -> {
                lyt_male.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_pink)
                iv_male.setImageResource(R.mipmap.gender_selected)
                mGenderPreferences = "male"
            }
            AppConstants.FEMALE -> {
                lyt_female.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_pink)
                iv_female.setImageResource(R.mipmap.gender_selected)
                mGenderPreferences = "female"
            }
            AppConstants.BOTH -> {
                lyt_both.background = ContextCompat.getDrawable(this@BioSeekingActivity, R.drawable.bg_pink)
                iv_both.setImageResource(R.mipmap.gender_selected)
                mGenderPreferences = "Both"
            }
        }
    }
}