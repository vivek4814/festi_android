package com.muzified.activity

import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.muzified.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import kotlinx.android.synthetic.main.toolbar_main.*


class TermsOfServiceActivity: AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
        tv_pageTitle.text = resources.getString(R.string.terms_of_services)
        val webSettings = activity_webView.getSettings()
        webSettings.javaScriptEnabled = true
        activity_webView.loadUrl("file:///android_asset/termsand_condition.html")
    }

    private fun setupClickListener()
    {
        iv_cancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_cancel -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
        }
    }
}