package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lorentzos.flingswipe.SwipeFlingAdapterView
import com.lorentzos.flingswipe.SwipeFlingAdapterView.onFlingListener
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.adapter.CardSwipeViewAdapter
import com.muzified.dialog.DialogUtils
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.SignUpModel
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.lyt_bar
import kotlinx.android.synthetic.main.activity_home.lyt_parent
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.toolbar_home.*
import link.fls.swipestack.SwipeStack
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList


class HomeActivity : AppCompatActivity(), View.OnClickListener, CardStackListener,
    ItemClickListenerWithViewType {

    private var mToolbarLayout: Toolbar? = null
    val db = Firebase.firestore
    private var mOtherUserProfileList: ArrayList<SignUpModel>? = null

    private var mManager: CardStackLayoutManager? = null
    private var mSwipeCardadapter: CardSwipeViewAdapter? = null
    private var mLoggedInUserData: SignUpModel? = null
    private var mCardSwipePosition: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupSwipeCardView()
        setupClickListener()
        getLoggedInUserInfo()
    }

    private fun setupSwipeCardView() {
        mOtherUserProfileList = ArrayList<SignUpModel>()
        mSwipeCardadapter = CardSwipeViewAdapter(this, mOtherUserProfileList!!, this)

        //set the listener and the adapter
        //swipe_cardView.setAdapter(mSwipeCardadapter)
        mManager = CardStackLayoutManager(this, this)

        mManager?.setStackFrom(StackFrom.None)
        mManager?.setVisibleCount(3)
        mManager?.setTranslationInterval(8.0f)
        mManager?.setScaleInterval(0.95f)
        mManager?.setSwipeThreshold(0.3f)
        mManager?.setMaxDegree(20.0f)
        mManager?.setDirections(Direction.HORIZONTAL)
        mManager?.setCanScrollHorizontal(true)
        mManager?.setCanScrollVertical(false)
        mManager?.setSwipeableMethod(SwipeableMethod.Manual)
        mManager?.setOverlayInterpolator(LinearInterpolator())
        swipe_cardView.layoutManager = mManager
        swipe_cardView.adapter = mSwipeCardadapter
        swipe_cardView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }


//        swipe_cardView.setFlingListener(object : onFlingListener {
//
//            override fun onAdapterAboutToEmpty(i: Int) {
//                // Ask for more data here
//                if (i == 0) {
////                    tv_noMoreResult.visibility = View.VISIBLE
////                    swipe_cardView.visibility = View.GONE
////                    tv_noMoreResult.text = "No more result available"
////                    ToastMsgUtils.showSuccessToastMsg(this@HomeActivity, "No more card")
//                }
//            }
//
//            override fun onLeftCardExit(p0: Any?) {
//            }
//
//            override fun onRightCardExit(p0: Any?) {
//                makeRequestForChat(p0 as SignUpModel)
//            }
//
//            override fun onScroll(p0: Float) {
//
//            }
//
//            override fun removeFirstObjectInAdapter() {
//                mOtherUserProfileList?.removeAt(0)
//                mSwipeCardadapter?.notifyDataSetChanged()
//            }
//        })

        // Optionally add an OnItemClickListener
//        swipe_cardView.setOnItemClickListener(object : SwipeFlingAdapterView.OnItemClickListener {
//            override fun onItemClicked(p0: Int, p1: Any?) {
//                val selectedOtherUserProfile = mOtherUserProfileList?.get(p0)
//                val profileIntent = Intent(this@HomeActivity, ViewProfileActivity::class.java)
//                profileIntent.putExtra(AppConstants.USER_ID, selectedOtherUserProfile?.user_id)
//                startActivity(profileIntent)
//                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
//            }
//        })
    }

    override fun onItemClick(position: Int, view: View) {
        val selectedOtherUserProfile = mOtherUserProfileList?.get(position)
        when (view.id) {
            R.id.iv_option -> {
                showBottomSheetDialog()
            }
            R.id.lyt_parent -> {
                val selectedOtherUserProfile = mOtherUserProfileList?.get(position)
                val profileIntent = Intent(this@HomeActivity, ViewProfileActivity::class.java)
                profileIntent.putExtra(AppConstants.USER_ID, selectedOtherUserProfile?.user_id)
                startActivity(profileIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
        }
    }

    private fun setupClickListener() {
        iv_toolbarMenu.setOnClickListener(this)
        iv_logoHome.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_logoHome ->
            {
                val signUpIntent = Intent(this@HomeActivity, ChatListActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
            R.id.iv_toolbarMenu -> {
                val signUpIntent = Intent(this@HomeActivity, HomeMenuActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }
    }

    private fun getLoggedInUserInfo() {
        if (NetworkUtils.isNetworkAvailable(this@HomeActivity)) {
            lyt_bar.visibility = View.VISIBLE
            val gender = Prefs.getString(AppConstants.GENDER_PREFERENCE)
            db.collection("users")
                .whereEqualTo("user_id", Prefs.getString(AppConstants.USER_ID))
                .get()
                .addOnSuccessListener { result ->
                    if (!result.isEmpty) {
                        val userModel = SignUpModel()
                        for (document in result) {
                            val userData = document.data
                            if (userData.contains("user_id") && !TextUtils.isEmpty(userData.get("user_id") as String)) {
                                userModel.user_id = userData.get("user_id") as String
                            }
                            if (userData.contains("isProfileCompleted") && userData.get("isProfileCompleted") != null) {
                                userModel.isProfileCompleted =
                                    userData.get("isProfileCompleted") as Boolean
                            }
                            if (userData.contains("first_name") && !TextUtils.isEmpty(userData.get("first_name") as String)) {
                                userModel.first_name = userData.get("first_name") as String
                            }
                            if (userData.contains("last_name") && !TextUtils.isEmpty(userData.get("last_name") as String)) {
                                userModel.last_name = userData.get("last_name") as String
                            }
                            if (userData.contains("dob") && !TextUtils.isEmpty(
                                    userData.get("dob").toString()
                                )
                            ) {
                                val dateStr = userData.get("dob").toString()
                                val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                                val m = p.matcher(dateStr)
                                val b: Boolean = m.find()

                                if (b) {
                                    //10/27/2021
                                    val convertedDateTime = DateUtils.convertStringToDate(
                                        userData.get("dob").toString()
                                    )
                                    userModel.dob = convertedDateTime
                                } else {
                                    userModel.dob = userData.get("dob") as Long
                                }

                            }
                            if (userData.contains("email") && !TextUtils.isEmpty(userData.get("email") as String)) {
                                userModel.email = userData.get("email") as String
                            }
                            if (userData.contains("gender") && !TextUtils.isEmpty(userData.get("gender") as String)) {
                                userModel.gender = userData.get("gender") as String
                            }
                            if (userData.contains("profile") && !TextUtils.isEmpty(userData.get("profile") as String)) {
                                userModel.profile = userData.get("profile") as String
                            }
                            if (userData.contains("bio") && !TextUtils.isEmpty(userData.get("bio") as String)) {
                                userModel.bio = userData.get("bio") as String
                            }
                            if (userData.contains("lat") && !TextUtils.isEmpty(userData.get("lat") as String)) {
                                userModel.lat = userData.get("lat") as String
                                Prefs.putString(
                                    AppConstants.USER_LATITUDE,
                                    userData.get("lat") as String
                                )
                            }
                            if (userData.contains("long") && !TextUtils.isEmpty(userData.get("long") as String)) {
                                userModel.long = userData.get("long") as String
                                Prefs.putString(
                                    AppConstants.USER_LONGITUDE,
                                    userData.get("long") as String
                                )
                            }
                            if (userData.contains("gender_preference") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gender_preference"
                                    ) as String
                                )
                            ) {
                                userModel.gender_preference =
                                    userData.get("gender_preference") as String
                                Prefs.putString(
                                    AppConstants.GENDER_PREFERENCE,
                                    userData.get("gender_preference") as String
                                )
                            }
                            if (userData.contains("gallery_image0") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image0"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image0 = userData.get("gallery_image0") as String
                            }
                            if (userData.contains("gallery_image1") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image1"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image1 = userData.get("gallery_image1") as String
                            }
                            if (userData.contains("gallery_image2") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image2"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image2 = userData.get("gallery_image2") as String
                            }
                            if (userData.contains("gallery_image3") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image3"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image3 = userData.get("gallery_image3") as String
                            }
                            if (userData.contains("gallery_image4") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image4"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image4 = userData.get("gallery_image4") as String
                            }
                            if (userData.contains("date_created") && !TextUtils.isEmpty(
                                    userData.get(
                                        "date_created"
                                    ).toString()
                                )
                            ) {
                                val dateStr = userData.get("date_created").toString()
                                val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                                val m = p.matcher(dateStr)
                                val b: Boolean = m.find()

                                if (b) {
                                    //10/27/2021
//                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
//                                userModel.dob = convertedDateTime
                                } else {
                                    userModel.date_created = userData.get("date_created") as Long
                                }

                            }
                            mLoggedInUserData = userModel
                            getOtherUserProfile()
                        }
                    } else {
                        lyt_bar.visibility = View.GONE
                        lyt_noResultFound.visibility = View.VISIBLE
                        lyt_resultFound.visibility = View.GONE
                    }
                }
                .addOnFailureListener { exception ->
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "" + exception.message)
                }
        } else {
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
        }
    }

    private fun makeRequestForChat(p0: SignUpModel?) {
        lyt_bar.visibility = View.VISIBLE
        val requestId = p0?.user_id + ":" + Prefs.getString(AppConstants.USER_ID)
        db.collection("request")
            .whereEqualTo("request_id", requestId)
            .get()
            .addOnSuccessListener { result ->
                if(result.isEmpty)
                {
                    val requestData: MutableMap<String, Any> = HashMap()
                    requestData["to_id"] = p0?.user_id!!
                    requestData["from_id"] = Prefs.getString(AppConstants.USER_ID)
                    requestData["first_name"] = mLoggedInUserData?.first_name!!
                    requestData["last_name"] = mLoggedInUserData?.last_name!!
                    requestData["from_email"] = mLoggedInUserData?.email!!
                    requestData["from_image"] = mLoggedInUserData?.profile!!
                    requestData["request_id"] = requestId

                    val usersRef = db.collection("request")
                    usersRef.document(requestId).set(requestData)
                        .addOnSuccessListener { documentReference ->
                            lyt_bar.visibility = View.GONE
                            ToastMsgUtils.showSuccessToastMsg(this@HomeActivity, "Request send successfully")
                        }
                        .addOnFailureListener { e ->
                            lyt_bar.visibility = View.GONE
                            Log.w("Failure", "Error adding document", e)
                        }
                }
                else
                {
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showSuccessToastMsg(this@HomeActivity, "You already sent him a request")
                }
            }
            .addOnFailureListener{
                lyt_bar.visibility = View.GONE
            }
    }

    private fun getOtherUserProfile() {
        db.collection("users")
            .whereEqualTo("gender_preference", Prefs.getString(AppConstants.GENDER_PREFERENCE))
            .get()
            .addOnSuccessListener { result ->
                if (!result.isEmpty) {
                    val otherUserProfileList = ArrayList<SignUpModel>()
                    for (document in result) {
                        val userData = document.data
                        val userModel = SignUpModel()
                        if (userData.contains("user_id") && !userData.get("user_id")?.equals(
                                Prefs.getString(
                                    AppConstants.USER_ID
                                )
                            )!!
                        ) {
                            if (userData.contains("user_id") && !TextUtils.isEmpty(
                                    userData.get(
                                        "user_id"
                                    ) as String
                                )
                            ) {
                                userModel.user_id = userData.get("user_id") as String
                            }
                            if (userData.contains("isProfileCompleted") && userData.get("isProfileCompleted") != null) {
                                userModel.isProfileCompleted =
                                    userData.get("isProfileCompleted") as Boolean
                            }
                            if (userData.contains("first_name") && !TextUtils.isEmpty(
                                    userData.get(
                                        "first_name"
                                    ) as String
                                )
                            ) {
                                userModel.first_name = userData.get("first_name") as String
                            }
                            if (userData.contains("last_name") && !TextUtils.isEmpty(
                                    userData.get(
                                        "last_name"
                                    ) as String
                                )
                            ) {
                                userModel.last_name = userData.get("last_name") as String
                            }

                            if (userData.contains("dob") && !TextUtils.isEmpty(
                                    userData.get("dob").toString()
                                )
                            ) {
                                val dateStr = userData.get("dob").toString()
                                val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                                val m = p.matcher(dateStr)
                                val b: Boolean = m.find()

                                if (b) {
                                    //10/27/2021
                                    val convertedDateTime = DateUtils.convertStringToDate(
                                        userData.get("dob").toString()
                                    )
                                    userModel.dob = convertedDateTime
                                } else {
                                    userModel.dob = userData.get("dob") as Long
                                }

                            }

                            if (userData.contains("email") && !TextUtils.isEmpty(userData.get("email") as String)) {
                                userModel.email = userData.get("email") as String
                            }
                            if (userData.contains("gender") && !TextUtils.isEmpty(userData.get("gender") as String)) {
                                userModel.gender = userData.get("gender") as String
                            }
                            if (userData.contains("profile") && !TextUtils.isEmpty(
                                    userData.get(
                                        "profile"
                                    ) as String
                                )
                            ) {
                                userModel.profile = userData.get("profile") as String
                            }
                            if (userData.contains("bio") && !TextUtils.isEmpty(userData.get("bio") as String)) {
                                userModel.bio = userData.get("bio") as String
                            }
                            if (userData.contains("lat") && !TextUtils.isEmpty(userData.get("lat") as String)) {
                                userModel.lat = userData.get("lat") as String
                            }
                            if (userData.contains("long") && !TextUtils.isEmpty(userData.get("long") as String)) {
                                userModel.long = userData.get("long") as String
                            }
                            if (userData.contains("gender_preference") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gender_preference"
                                    ) as String
                                )
                            ) {
                                userModel.gender_preference =
                                    userData.get("gender_preference") as String
                            }
                            if (userData.contains("gallery_image0") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image0"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image0 = userData.get("gallery_image0") as String
                            }
                            if (userData.contains("gallery_image1") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image1"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image1 = userData.get("gallery_image1") as String
                            }
                            if (userData.contains("gallery_image2") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image2"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image2 = userData.get("gallery_image2") as String
                            }
                            if (userData.contains("gallery_image3") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image3"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image3 = userData.get("gallery_image3") as String
                            }
                            if (userData.contains("gallery_image4") && !TextUtils.isEmpty(
                                    userData.get(
                                        "gallery_image4"
                                    ) as String
                                )
                            ) {
                                userModel.gallery_image4 = userData.get("gallery_image4") as String
                            }
                            if (userData.contains("favourites_artist_ids") && userData.get("favourites_artist_ids") != null) {
                                userModel.favourites_artist_ids =
                                    userData.get("favourites_artist_ids") as ArrayList<String>
                            }
                            if (userData.contains("favourites_music_ids") && userData.get("favourites_music_ids") != null) {
                                userModel.favourites_music_ids =
                                    userData.get("favourites_music_ids") as ArrayList<String>
                            }
//                                if (userData.contains("date_created") && userData.get("date_created") != null) {
//                                    userModel.date_created = userData.get("date_created") as Long
//                                }
                            if (userData.contains("date_created") && !TextUtils.isEmpty(
                                    userData.get(
                                        "date_created"
                                    ).toString()
                                )
                            ) {
                                val dateStr = userData.get("date_created").toString()
                                val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                                val m = p.matcher(dateStr)
                                val b: Boolean = m.find()

                                if (b) {
                                    //10/27/2021
//                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
//                                userModel.dob = convertedDateTime
                                } else {
                                    userModel.date_created = userData.get("date_created") as Long
                                }

                            }
                            otherUserProfileList.add(userModel)
                        }
                    }
                    lyt_bar.visibility = View.GONE
                    if (!otherUserProfileList.isNullOrEmpty()) {
                        lyt_noResultFound.visibility = View.GONE
                        lyt_resultFound.visibility = View.VISIBLE
                        if (!mOtherUserProfileList.isNullOrEmpty()) {
                            mOtherUserProfileList?.clear()
                        }
                        mOtherUserProfileList?.addAll(otherUserProfileList)
                        mSwipeCardadapter?.notifyDataSetChanged()
                        Log.e("ListSize", mOtherUserProfileList?.size!!.toString())
                    } else {
                        lyt_noResultFound.visibility = View.VISIBLE
                        lyt_resultFound.visibility = View.GONE
                    }
                } else {
                    lyt_bar.visibility = View.GONE
                    lyt_noResultFound.visibility = View.VISIBLE
                    lyt_resultFound.visibility = View.GONE
                }
            }
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
            }
    }

    private fun showBottomSheetDialog() {
        val bottomSheetDialog =
            BottomSheetDialog(this@HomeActivity, R.style.CustomBottomSheetDialogTheme)
        val sheetView: View = layoutInflater.inflate(R.layout.dialog_flag_cancel, null)
        bottomSheetDialog.setContentView(sheetView)
        bottomSheetDialog.show()
        val flagBtn = bottomSheetDialog.findViewById<CardView>(R.id.lyt_flag)
        val cancelBtn = bottomSheetDialog.findViewById<CardView>(R.id.lyt_dismissDialog)
        flagBtn?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                bottomSheetDialog.dismiss()
                DialogUtils.openAlertDialog(
                    this@HomeActivity,
                    "Thank you for flagging this content"
                )
            }
        })
        cancelBtn?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                bottomSheetDialog.dismiss()
            }
        })
        bottomSheetDialog.show()
    }

//    override fun onViewSwipedToLeft(position: Int) {
////        mOtherUserProfileList?.removeAt(position)
////        mSwipeCardadapter?.notifyDataSetChanged()
//    }
//
//    override fun onViewSwipedToRight(position: Int) {
//        val signUpModel = mOtherUserProfileList?.get(position)
//        makeRequestForChat(signUpModel)
//        mOtherUserProfileList?.removeAt(position)
//        mSwipeCardadapter?.notifyDataSetChanged()
//    }
//
//    override fun onStackEmpty() {
//        tv_noMoreResult.visibility = View.VISIBLE
//        swipe_cardView.visibility = View.GONE
//        tv_noMoreResult.text = "No more result available"
//        ToastMsgUtils.showSuccessToastMsg(this@HomeActivity, "No more card")
//    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
        mCardSwipePosition = direction?.name!!
    }

    override fun onCardSwiped(direction: Direction?) {
    }

    override fun onCardRewound() {
    }

    override fun onCardCanceled() {
    }

    override fun onCardAppeared(view: View?, position: Int) {
    }

    override fun onCardDisappeared(view: View?, position: Int) {
        val signUpModel = mOtherUserProfileList?.get(position)
        if (!TextUtils.isEmpty(mCardSwipePosition))
        {
            if (mCardSwipePosition.equals("Right"))
            {
                makeRequestForChat(signUpModel)
            }
        }
    }
}