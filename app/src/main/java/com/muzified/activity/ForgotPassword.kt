package com.muzified.activity

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.muzified.R
import com.muzified.Utils.NetworkUtils
import com.muzified.Utils.ToastMsgUtils
import com.muzified.Utils.Utility
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.toolbar_main.*

class ForgotPassword : AppCompatActivity(), View.OnClickListener  {

    private var mToolbarLayout: Toolbar? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun setupUI()
    {
        val spannable1 = SpannableString("Forgot to ")
        spannable1.setSpan(ForegroundColorSpan(Color.parseColor("#ffffff")), 0, "Forgot to ".length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_forgotTitle.text = spannable1
        val spannable2 = SpannableString("Muzified")
        spannable2.setSpan(ForegroundColorSpan(Color.parseColor("#E53935")), 0, "Muzified".length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_forgotTitle.append(spannable2)
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
        btn_send.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.btn_send ->
            {
                Utility.hideKeyboard(this, btn_send)
                if (NetworkUtils.isNetworkAvailable(this@ForgotPassword)) {
                    if (isValidate()) {
                        sendPasswordResetEmail()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, resources.getString(R.string.error_msg_empty_email))
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString().trim()).matches()) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, resources.getString(R.string.error_msg_invalid_email))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun sendPasswordResetEmail() {
        lyt_bar.visibility = View.VISIBLE
        mAuth?.sendPasswordResetEmail(et_email.text.toString().trim())!!
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    lyt_bar.visibility = View.GONE
                    showAlertDialog()
                } else {
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "Failed to send reset email!")
                }
            })
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setMessage("We have sent you instructions to reset your password!\n\nPlease check your mail and reset the password.")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                finish()
            })
        // create dialog box
        val alert = alertDialog.create()
        // set title for alert dialog box
        alert.setTitle("Alert")
        // show alert dialog
        alert.show()
    }
}