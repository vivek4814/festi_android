package com.muzified.activity

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.muzified.R
import com.muzified.Utils.AlertDialogUtils
import com.muzified.Utils.AppConstants
import com.muzified.Utils.Utility
import com.muzified.adapter.DistanceStepCountAdapter
import com.muzified.model.DistanceStepBean
import com.muzified.model.SignUpModel
import com.vinay.stepview.models.Step
import com.zhy.view.flowlayout.FlowLayout
import com.zhy.view.flowlayout.TagAdapter
import kotlinx.android.synthetic.main.activity_swipe_preferences.*
import kotlinx.android.synthetic.main.activity_view_profile.*
import kotlinx.android.synthetic.main.toolbar_main.*

class SwipingPreferenceActivity : AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null
    private var mLoggedInUserData: SignUpModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe_preferences)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupStepView()
        setupFavoriteArtistView()
        setupFavoriteMusicView()
        setupClickListener()
    }

    private fun initialiseUI() {
        if (intent.getSerializableExtra(AppConstants.SIGN_UP_USER_DATA) != null)
        {
            mLoggedInUserData = intent.getSerializableExtra(AppConstants.SIGN_UP_USER_DATA) as SignUpModel
        }
        tv_toolbarTitle.text = ""

        if (!TextUtils.isEmpty(mLoggedInUserData?.gender))
        {
            val gender = mLoggedInUserData?.gender
            when(gender)
            {
                "Male" ->{
                    radioBtn_male.isChecked = true
                    radioBtn_male.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
                "male" ->{
                    radioBtn_male.isChecked = true
                    radioBtn_male.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
                "Female" ->{
                    radioBtn_female.isChecked = true
                    radioBtn_female.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
                "female" ->{
                    radioBtn_female.isChecked = true
                    radioBtn_female.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
                "Both" ->{
                    radioBtn_both.isChecked = true
                    radioBtn_both.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
                "both" ->{
                    radioBtn_both.isChecked = true
                    radioBtn_both.buttonDrawable = ContextCompat.getDrawable(this, R.mipmap.selected)
                }
            }
        }

        if (!TextUtils.isEmpty(mLoggedInUserData?.lat) && !TextUtils.isEmpty(mLoggedInUserData?.long))
        {
            val locationAddress = Utility.getCompleteAddressFromLatLong(this@SwipingPreferenceActivity, mLoggedInUserData?.lat?.toDouble()!!, mLoggedInUserData?.long?.toDouble()!!)
            tv_location.text = locationAddress
        }
    }

    private fun setupFavoriteArtistView() {
        val favouriteArtistList = AppConstants.getArtistList()
        val userFavouriteArtistIds = mLoggedInUserData?.favourites_artist_ids
        val favoriteArtist = ArrayList<String>()
        if (!userFavouriteArtistIds.isNullOrEmpty())
        {
            if (!favouriteArtistList.isNullOrEmpty())
            {
                for (j in 0 until userFavouriteArtistIds.size)
                {
                    val favouriteArtistId = userFavouriteArtistIds.get(j)
                    for (i in 0 until favouriteArtistList.size)
                    {
                        val artistModel = favouriteArtistList.get(i)
                        if (favouriteArtistId.equals(artistModel.id)){
                            favoriteArtist.add(artistModel.FavoriteArtistName)
                            break
                        }
                    }
                }
            }
        }

        artist_flowLayout.setAdapter(object : TagAdapter<String>(favoriteArtist) {
            override fun getView(
                parent: FlowLayout,
                position: Int,
                artist: String
            ): View {
                var view: View? = null
                val mInflater = LayoutInflater.from(this@SwipingPreferenceActivity)
                if (view == null) {
                    view = mInflater.inflate(
                        R.layout.item_tag_flow_list,
                        artist_flowLayout,
                        false
                    )
                }
                val mArtistTextView = view?.findViewById<View>(R.id.tv_name) as TextView
                //val mMusic: String = showProfileTagBean.getName()
                mArtistTextView.text = artist
                return view
            }
        })
    }

    private fun setupFavoriteMusicView() {
        val favouriteMusicList = AppConstants.getFavouriteMusicList()
        val userFavouriteMusicIds = mLoggedInUserData?.favourites_music_ids
        val favoriteMusic = ArrayList<String>()

        if (!userFavouriteMusicIds.isNullOrEmpty())
        {
            for (j in 0 until userFavouriteMusicIds.size)
            {
                val favouriteMusicId = userFavouriteMusicIds.get(j)
                for (i in 0 until favouriteMusicList.size)
                {
                    val musicModel = favouriteMusicList.get(i)
                    if (favouriteMusicId.equals(musicModel.id)){
                        favoriteMusic.add(musicModel.FavoriteMusicName)
                        break
                    }
                }
            }
        }

        music_flowLayout.setAdapter(object : TagAdapter<String>(favoriteMusic) {
            override fun getView(
                parent: FlowLayout,
                position: Int,
                music: String
            ): View {
                var view: View? = null
                val mInflater = LayoutInflater.from(this@SwipingPreferenceActivity)
                if (view == null) {
                    view = mInflater.inflate(
                        R.layout.item_tag_flow_list,
                        music_flowLayout,
                        false
                    )
                }
                val mMusicTextView = view?.findViewById<View>(R.id.tv_name) as TextView
                //val mMusic: String = showProfileTagBean.getName()
                mMusicTextView.text = music
                return view
            }
        })
    }

    private fun setupStepView() {
        val stepList = ArrayList<Step>()
        stepList.add(Step("0 km", Step.State.COMPLETED))
        stepList.add(Step("1 km", Step.State.COMPLETED))
        stepList.add(Step("2 km", Step.State.COMPLETED))
        stepList.add(Step("3 km", Step.State.NOT_COMPLETED))
        stepList.add(Step("4 km", Step.State.NOT_COMPLETED))
        stepList.add(Step("5 km", Step.State.NOT_COMPLETED))

        stepsView.setSteps(stepList)

        // Drawables
        stepsView.setCompletedStepIcon(resources.getDrawable(R.mipmap.circle_pink_small)!!)
        stepsView.setCurrentStepIcon(resources.getDrawable(R.mipmap.circle_pink_large)!!)
        stepsView.setNotCompletedStepIcon(resources.getDrawable(R.mipmap.circle_white)!!)

        //Text
        stepsView.setCompletedStepTextColor(resources.getColor(R.color.colorBlack2))
        stepsView.setCurrentStepTextColor(resources.getColor(R.color.colorBlack2))
        stepsView.setNotCompletedStepTextColor(resources.getColor(R.color.colorBlack2))

        // Line colors
        stepsView.setCompletedLineColor(resources.getColor(R.color.colorPink))
        stepsView.setNotCompletedLineColor(resources.getColor(R.color.colorWhite))
        // Text size (in sp)
        stepsView.setTextSize(12)
        stepsView.setCircleRadius(5F)
        stepsView.setLineLength(50F)
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
        }
    }
}