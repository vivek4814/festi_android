package com.muzified.activity

import android.app.Application
import android.content.ContextWrapper
import com.google.firebase.FirebaseApp
import com.muzified.Utils.Prefs

class MuzifiedApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}