package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.model.SignUpModel
import kotlinx.android.synthetic.main.activity_confirm_profile.*
import kotlinx.android.synthetic.main.activity_home_menu.*
import kotlinx.android.synthetic.main.activity_home_menu.lyt_bar
import kotlinx.android.synthetic.main.activity_home_menu.lyt_parent
import kotlinx.android.synthetic.main.activity_home_menu.tv_userName
import java.util.regex.Pattern

class HomeMenuActivity: AppCompatActivity(), View.OnClickListener {

    val db = Firebase.firestore
    private var mLoggedInUserData: SignUpModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_menu)
        setupClickListener()
        getLoggedInUserData()
    }

    private fun setupClickListener()
    {
        iv_cancel.setOnClickListener(this)
        profile_image.setOnClickListener(this)

        tv_chatList.setOnClickListener(this)
        tv_chatRequest.setOnClickListener(this)
        tv_swipingPreferences.setOnClickListener(this)
        tv_rateUs.setOnClickListener(this)
        tv_privacyPolicy.setOnClickListener(this)
        tv_termsOfServices.setOnClickListener(this)
        tv_contactUs.setOnClickListener(this)
        tv_logOut.setOnClickListener(this)
        tv_closeAccount.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_cancel ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.profile_image ->
            {

            }
            R.id.tv_chatList ->
            {
                launchActivity(AppConstants.CHAT_LIST)
            }
            R.id.tv_chatRequest ->
            {
                launchActivity(AppConstants.CHAT_REQUEST)
            }
            R.id.tv_swipingPreferences ->
            {
                launchActivity(AppConstants.SWIPING_PREFERENCES)
            }
            R.id.tv_rateUs ->
            {
                launchActivity(AppConstants.RATE_US)
            }
            R.id.tv_privacyPolicy ->
            {
                launchActivity(AppConstants.PRIVACY_POLICY)
            }
            R.id.tv_termsOfServices ->
            {
                launchActivity(AppConstants.TERMS_OF_SERVICE)
            }
            R.id.tv_contactUs ->
            {
                launchActivity(AppConstants.CONTACT_US)
            }
            R.id.tv_logOut ->
            {
                AlertDialogUtils.showAlertDialog(this, "Alert!", "You will now be logged out of Muzified", "Ok", "Cancel", AppConstants.LOGOUT_TYPE)

            }
            R.id.tv_closeAccount ->
            {
                AlertDialogUtils.showAlertDialog(this, "Alert!", "You will now be delete account of Muzified", "Ok", "Cancel", AppConstants.CLOSE_ACCOUNT_TYPE)

            }
        }
    }

    private fun launchActivity(type: String)
    {
        when(type)
        {
            AppConstants.USER_PROFILE ->
            {
            }
            AppConstants.CHAT_LIST ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, ChatListActivity::class.java)
                startActivity(signUpIntent)
            }
            AppConstants.CHAT_REQUEST ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, ChatRequestActivity::class.java)
                startActivity(signUpIntent)
            }
            AppConstants.SWIPING_PREFERENCES ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, SwipingPreferenceActivity::class.java)
                signUpIntent.putExtra(AppConstants.SIGN_UP_USER_DATA, mLoggedInUserData)
                startActivity(signUpIntent)
            }
            AppConstants.PRIVACY_POLICY ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, PrivacyPolicyActivity::class.java)
                startActivity(signUpIntent)
            }
            AppConstants.TERMS_OF_SERVICE ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, TermsOfServiceActivity::class.java)
                startActivity(signUpIntent)
            }
            AppConstants.CONTACT_US ->
            {
                val signUpIntent = Intent(this@HomeMenuActivity, ContactUsActivity::class.java)
                startActivity(signUpIntent)
            }
        }
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private fun getLoggedInUserData()
    {
        lyt_bar.visibility = View.VISIBLE
        db.collection("users")
            .whereEqualTo("user_id", Prefs.getString(AppConstants.USER_ID))
            .get()
            .addOnSuccessListener { result ->
                if (!result.isEmpty) {
                    val userModel = SignUpModel()
                    for (document in result) {
                        val userData = document.data
                        if (userData.contains("user_id") && !TextUtils.isEmpty(userData.get("user_id") as String)) {
                            userModel.user_id = userData.get("user_id") as String
                        }
                        if (userData.contains("isProfileCompleted") && userData.get("isProfileCompleted") != null) {
                            userModel.isProfileCompleted =
                                userData.get("isProfileCompleted") as Boolean
                        }
                        if (userData.contains("first_name") && !TextUtils.isEmpty(userData.get("first_name") as String)) {
                            userModel.first_name = userData.get("first_name") as String
                        }
                        if (userData.contains("last_name") && !TextUtils.isEmpty(userData.get("last_name") as String)) {
                            userModel.last_name = userData.get("last_name") as String
                        }
                        if (userData.contains("dob") && !TextUtils.isEmpty(userData.get("dob").toString()))
                        {
                            val dateStr = userData.get("dob").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.dob = userData.get("dob") as Long
                            }

                        }
                        if (userData.contains("email") && !TextUtils.isEmpty(userData.get("email") as String)) {
                            userModel.email = userData.get("email") as String
                        }
                        if (userData.contains("gender") && !TextUtils.isEmpty(userData.get("gender") as String)) {
                            userModel.gender = userData.get("gender") as String
                        }
                        if (userData.contains("profile") && !TextUtils.isEmpty(userData.get("profile") as String)) {
                            userModel.profile = userData.get("profile") as String
                        }
                        if (userData.contains("bio") && !TextUtils.isEmpty(userData.get("bio") as String)) {
                            userModel.bio = userData.get("bio") as String
                        }
                        if (userData.contains("lat") && !TextUtils.isEmpty(userData.get("lat") as String)) {
                            userModel.lat = userData.get("lat") as String
                        }
                        if (userData.contains("long") && !TextUtils.isEmpty(userData.get("long") as String)) {
                            userModel.long = userData.get("long") as String
                        }
                        if (userData.contains("gender_preference") && !TextUtils.isEmpty(
                                userData.get(
                                    "gender_preference"
                                ) as String
                            )
                        ) {
                            userModel.gender_preference =
                                userData.get("gender_preference") as String
                        }
                        if (userData.contains("gallery_image0") && !TextUtils.isEmpty(userData.get("gallery_image0") as String)) {
                            userModel.gallery_image0 = userData.get("gallery_image0") as String
                        }
                        if (userData.contains("gallery_image1") && !TextUtils.isEmpty(userData.get("gallery_image1") as String)) {
                            userModel.gallery_image1 = userData.get("gallery_image1") as String
                        }
                        if (userData.contains("gallery_image2") && !TextUtils.isEmpty(userData.get("gallery_image2") as String)) {
                            userModel.gallery_image2 = userData.get("gallery_image2") as String
                        }
                        if (userData.contains("gallery_image3") && !TextUtils.isEmpty(userData.get("gallery_image3") as String)) {
                            userModel.gallery_image3 = userData.get("gallery_image3") as String
                        }
                        if (userData.contains("gallery_image4") && !TextUtils.isEmpty(userData.get("gallery_image4") as String)) {
                            userModel.gallery_image4 = userData.get("gallery_image4") as String
                        }
                        if (userData.contains("favourites_artist_ids") && userData.get("favourites_artist_ids") != null) {
                            userModel.favourites_artist_ids =
                                userData.get("favourites_artist_ids") as ArrayList<String>
                        }
                        if (userData.contains("favourites_music_ids") && userData.get("favourites_music_ids") != null) {
                            userModel.favourites_music_ids =
                                userData.get("favourites_music_ids") as ArrayList<String>
                        }
                        if (userData.contains("date_created") && !TextUtils.isEmpty(userData.get("date_created").toString()))
                        {
                            val dateStr = userData.get("date_created").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
//                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
//                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.date_created = userData.get("date_created") as Long
                            }

                        }
                    }
                    lyt_bar.visibility = View.GONE
                    mLoggedInUserData = userModel
                    updateUI()
                } else {
                    lyt_bar.visibility = View.GONE
                }
            }
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, "" + exception.message)
            }
    }

    private fun updateUI()
    {
        if(!TextUtils.isEmpty(mLoggedInUserData?.profile))
        {
            Glide.with(this@HomeMenuActivity)
                .load(mLoggedInUserData?.profile)
                .centerCrop()
                .placeholder(R.drawable.place_holder)
                .into(profile_image)
        }
        if (!TextUtils.isEmpty(mLoggedInUserData?.first_name))
        {
            tv_userName.text = mLoggedInUserData?.first_name + " " + mLoggedInUserData?.last_name
        }
        else
        {
            tv_userName.text = ""
        }
    }
}