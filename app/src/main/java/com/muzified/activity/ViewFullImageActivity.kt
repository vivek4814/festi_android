package com.muzified.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.model.SignUpModel
import kotlinx.android.synthetic.main.toolbar_main.*
import kotlinx.android.synthetic.main.activity_view_full_image.*

class ViewFullImageActivity: AppCompatActivity() , View.OnClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_full_image)

        setupUI()
        setupClickListener()
    }

    private fun setupUI()
    {
        if (intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_USER_DATA) != null)
        {
            val mSelectedUserData = intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_USER_DATA) as SignUpModel
            tv_toolbarTitle.text = mSelectedUserData?.first_name + " " + mSelectedUserData?.last_name
        }

        if (intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_MSG) != null)
        {
            val mSelectedImageLink = intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_MSG) as String
            Glide.with(this)
                .load(mSelectedImageLink)
                .centerCrop()
                .placeholder(R.mipmap.gallery_placeholder)
                .into(iv_fullImage)
        }
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->{
                finish()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}