package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.muzified.R
import kotlinx.android.synthetic.main.activity_privacy_policy.*

class PrivacyPolicyActivity: AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
        tv_pageTitle.text = resources.getString(R.string.privacy_policy)
        val webSettings = activity_webView.getSettings()
        webSettings.javaScriptEnabled = true
        activity_webView.loadUrl("file:///android_asset/privacy_policy.html")
    }

    private fun setupClickListener()
    {
        iv_cancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_cancel ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
        }
    }
}