package com.muzified.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.muzified.BuildConfig
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.Prefs
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = "LocationProvider"
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    private var mMediaPlayer: MediaPlayer? = null

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    /**
     * Represents a geographical location.
     */
    protected var mLastLocation: Location? = null

    private var mLatitudeLabel: String? = null
    private var mLongitudeLabel: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        setupClickListener()
    }

    override fun onResume() {
        super.onResume()
        playAudioFile()
    }

    private fun playAudioFile()
    {
        try {
            mMediaPlayer = MediaPlayer()
            val descriptor = assets.openFd("in_abstract_loop.mp3")
            mMediaPlayer?.setDataSource(descriptor.fileDescriptor, descriptor.startOffset, descriptor.length)
            descriptor.close()
            mMediaPlayer?.prepare()
            mMediaPlayer?.setVolume(1f, 1f)
            mMediaPlayer?.setLooping(true)
            mMediaPlayer?.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setupClickListener()
    {
        btn_continue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.btn_continue -> {
                if (!TextUtils.isEmpty(Prefs.getString(AppConstants.USER_ID))) {
                    val signUpIntent = Intent(this@WelcomeActivity, HomeActivity::class.java)
                    signUpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(signUpIntent)
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                } else {
                    val signUpIntent = Intent(
                        this@WelcomeActivity,
                        AccountSelectionActivity::class.java
                    )
                    startActivity(signUpIntent)
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
                releaseAudioPlayer()
            }
        }
    }

    public override fun onStart() {
        super.onStart()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient?.lastLocation!!
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result
                    Prefs.putString(
                        AppConstants.USER_LATITUDE,
                        mLastLocation?.latitude!!.toString()
                    )
                    Prefs.putString(
                        AppConstants.USER_LONGITUDE,
                        mLastLocation?.longitude!!.toString()
                    )
                    Log.w(TAG, "Latitude: -> " + mLastLocation?.latitude!!.toString())
                    Log.w(TAG, "Longitude: -> " + mLastLocation?.longitude!!.toString())
                } else {
                    Log.w(TAG, "getLastLocation:exception", task.exception)
                }
            }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this@WelcomeActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            showPermissionRationalDialog(
                this@WelcomeActivity,
                "Alert",
                getString(R.string.permission_rationale)
            )
        } else {
            Log.i("TAG", "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i("TAG", "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showOpenSettinglDialog(
                    this@WelcomeActivity, getString(R.string.settings), getString(
                        R.string.permission_denied_explanation
                    )
                )
            }
        }
    }

    fun showPermissionRationalDialog(from: Activity, title: String, msg: String)
    {
        AlertDialog.Builder(from)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton("OK", { dialog, _ ->
                startLocationPermissionRequest()
            })
            .show()
    }

    fun showOpenSettinglDialog(from: Activity, title: String, msg: String)
    {
        AlertDialog.Builder(from)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton("OK", { dialog, _ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts(
                    "package",
                    BuildConfig.APPLICATION_ID, null
                )
                intent.data = uri
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            })
            .show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        releaseAudioPlayer()
        finish()
    }

    private fun releaseAudioPlayer()
    {
        if (mMediaPlayer?.isPlaying()!!) {
            mMediaPlayer?.stop()
            mMediaPlayer?.release()
        }
    }
}