package com.muzified.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.adapter.ChatUserListAdapter
import com.muzified.listener.ItemClickListener
import com.muzified.model.SignUpModel
import com.muzified.model.conversation.ConversationResultModel
import kotlinx.android.synthetic.main.activity_chat_list.*
import kotlinx.android.synthetic.main.toolbar_main.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatListActivity: AppCompatActivity(), View.OnClickListener, ItemClickListener {

    val db = Firebase.firestore

    private var TAG = "ChatListActivity"
    private var mToolbarLayout: Toolbar? = null
    private var mChatUserList: ArrayList<ConversationResultModel>? = null
    private var mChatUserAdapter: ChatUserListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
        getChatUserCollectionList()
    }

    private fun initialiseUI()
    {
        tv_toolbarTitle.text = resources.getString(R.string.chat_list)
        mChatUserList = ArrayList<ConversationResultModel>()
        mChatUserAdapter = ChatUserListAdapter(this, mChatUserList!!, this)
        val mLayoutManager = LinearLayoutManager(this@ChatListActivity)
        rv_chatUserList.layoutManager = mLayoutManager
        rv_chatUserList.adapter = mChatUserAdapter
        rv_chatUserList.hasFixedSize()

        et_searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable?.length!! > 0)
                {
                    iv_clear.visibility = View.VISIBLE
                }
                else
                {
                    iv_clear.visibility = View.GONE
                }
                mChatUserAdapter?.filter?.filter(editable)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    override fun onResume() {
        super.onResume()
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
        iv_clear?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
            R.id.iv_clear ->
            {
                et_searchView.setText("")
                et_searchView.hideKeyboard()
                iv_clear.visibility = View.GONE
                mChatUserAdapter?.filter?.filter("")
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun getChatUserCollectionList() {
        if (NetworkUtils.isNetworkAvailable(this@ChatListActivity)) {
            fetchRecord()
        } else {
            ToastMsgUtils.showSuccessMsg(
                lyt_parent!!,
                getString(R.string.error_msg_no_network)
            )
        }
    }

    private fun fetchRecord() {
        lyt_bar?.visibility = View.VISIBLE
        db.collection("Conversations")
            .whereArrayContains("userIDs", Prefs.getString(AppConstants.USER_ID))
            .addSnapshotListener { snapshots, e ->
                if (e != null) {
                    return@addSnapshotListener
                }

                if (snapshots?.documents?.isEmpty()!!) {
                    lyt_bar?.visibility = View.GONE
                    tv_noResultFound?.visibility = View.VISIBLE
                    lyt_resultFound?.visibility = View.GONE
                } else {
//                    if (!mChatUserList?.isNullOrEmpty()!!) {
//                        mChatUserList?.clear()
//                        mChatUserAdapter?.notifyDataSetChanged()
//                    }
                }

                for (data in snapshots.documentChanges) {
                    when (data.type) {
                        DocumentChange.Type.ADDED -> {
                            updateResult(data, "ADDED")
                        }
                        DocumentChange.Type.MODIFIED -> {
                            updateResult(data, "MODIFIED")
                            Log.d(TAG, "Modified city: ${data.document.data}")
                        }
                        DocumentChange.Type.REMOVED -> {
                            updateResult(data, "REMOVED")
                            Log.d(TAG, "Removed city: ${data.document.data}")
                        }
                    }
                }
            }
    }

    private fun updateResult(data: DocumentChange, s: String) {
        when (s) {
            "ADDED" -> {
                val conversationResult = ConversationResultModel()
                conversationResult.timestamp = data.document.get("timestamp") as Long
                if (data.document.contains("id") && !TextUtils.isEmpty(
                        data.document.get(
                            "id"
                        ) as String
                    )
                ) {
                    conversationResult.id = data.document.get("id") as String
                }
                if (data.document.contains("isRead") && !(data.document.get("isRead") as HashMap<*, *>).isNullOrEmpty()) {
                    conversationResult.isRead = data.document.get("isRead") as HashMap<*, *>
                }
                if (data.document.contains("userIDs")) {
                    conversationResult.userIDs = data.document.get("userIDs") as ArrayList<String>
                }
                if (data.document.contains("lastMessage")) {
                    conversationResult.lastMessage = data.document.get("lastMessage") as String
                }

                mChatUserList?.add(conversationResult)
            }
            "MODIFIED" ->{
                val conversationResult = ConversationResultModel()
                conversationResult.timestamp = data.document.get("timestamp") as Long
                if (data.document.contains("id") && !TextUtils.isEmpty(
                        data.document.get(
                            "id"
                        ) as String
                    )
                ) {
                    conversationResult.id = data.document.get("id") as String
                }
                if (data.document.contains("isRead") && !(data.document.get("isRead") as HashMap<*, *>).isNullOrEmpty()) {
                    conversationResult.isRead = data.document.get("isRead") as HashMap<*, *>
                }
                if (data.document.contains("userIDs")) {
                    conversationResult.userIDs = data.document.get("userIDs") as ArrayList<String>
                }
                if (data.document.contains("lastMessage")) {
                    conversationResult.lastMessage = data.document.get("lastMessage") as String
                }

                for (i in 0 until mChatUserList?.size!!)
                {
                    val item = mChatUserList?.get(i)
                    if (item?.id == conversationResult.id)
                    {
                        item.lastMessage = conversationResult.lastMessage
                        mChatUserAdapter?.notifyItemRemoved(i)
                        break
                    }
                }
            }
            "REMOVED" -> {
                val id = data.document.get("id") as String
                for (i in 0 until mChatUserList?.size!!) {
                    val item = mChatUserList?.get(i)
                    if (item?.id.equals(id)) {
                        mChatUserList?.removeAt(i)
                        Collections.sort(
                            mChatUserList,
                            object : Comparator<ConversationResultModel> {
                                override fun compare(
                                    p0: ConversationResultModel?,
                                    p1: ConversationResultModel?
                                ): Int {
                                    return p1?.timestamp!!.compareTo(p0?.timestamp!!)
                                }
                            })
                        mChatUserAdapter?.notifyItemRemoved(i)
                        break
                    }
                }
            }
        }

        if (!mChatUserList.isNullOrEmpty()) {
            //Get Chat User Profile
            for (i in 0 until mChatUserList?.size!!) {
                val conversationResult = mChatUserList?.get(i)
                val chatUserIds = conversationResult?.userIDs
                if (!chatUserIds.isNullOrEmpty()) {
                    if (!chatUserIds[0].equals(Prefs.getString(AppConstants.USER_ID))) {
                        getUserProfileResult(chatUserIds[0], conversationResult)
                    } else {
                        getUserProfileResult(chatUserIds[1], conversationResult)
                    }
                }
            }
        }
    }

    private fun getUserProfileResult(userId: String, conversationResult: ConversationResultModel) {
        val userDataResult = SignUpModel()
        db.collection("users")
            .whereEqualTo("user_id", userId)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val userData = document.data
                    userDataResult.first_name = userData.get("first_name") as String
                    userDataResult.last_name = userData.get("last_name") as String
                    userDataResult.email = userData.get("email") as String
                    userDataResult.profile = userData.get("profile") as String
                    conversationResult.userProfileModel = userDataResult
                }
                Collections.sort(mChatUserList, object : Comparator<ConversationResultModel> {
                    override fun compare(
                        p0: ConversationResultModel?,
                        p1: ConversationResultModel?
                    ): Int {
                        return p1?.timestamp!!.compareTo(p0?.timestamp!!)
                    }
                })
                lyt_bar?.visibility = View.GONE
                tv_noResultFound?.visibility = View.GONE
                lyt_resultFound?.visibility = View.VISIBLE
                mChatUserAdapter?.notifyDataSetChanged()
            }
            .addOnFailureListener { exception ->
                lyt_bar?.visibility = View.GONE
                Log.e(TAG, exception.message!!)
            }
    }

    override fun onItemClick(position: Int) {
        val conversationId = mChatUserList?.get(position)
        val chatMessageIntent = Intent(this@ChatListActivity, ChatMessageActivity::class.java)
        chatMessageIntent.putExtra(AppConstants.SELECTED_CONVERSATION_ID, conversationId)
        startActivity(chatMessageIntent)
    }
}