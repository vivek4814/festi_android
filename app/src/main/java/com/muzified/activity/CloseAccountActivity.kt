package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.muzified.R
import kotlinx.android.synthetic.main.toolbar_home.*
import kotlinx.android.synthetic.main.activity_home.*

class CloseAccountActivity: AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupClickListener()
    }

    private fun setupClickListener()
    {
        iv_toolbarMenu.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarMenu ->
            {
                val signUpIntent = Intent(this@CloseAccountActivity, HomeMenuActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
//            R.id.lyt_btn_edit ->
//            {
//                val signUpIntent = Intent(this@ConfirmProfileActivity, FavoriteMusicActivity::class.java)
//                startActivity(signUpIntent)
//                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//            }
//            R.id.lyt_btn_confirm ->
//            {
//                val signUpIntent = Intent(this@ConfirmProfileActivity, FavoriteMusicActivity::class.java)
//                startActivity(signUpIntent)
//                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
//            }
        }
    }
}