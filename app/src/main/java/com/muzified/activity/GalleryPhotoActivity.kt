package com.muzified.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.muzified.R
import com.muzified.Utils.*
import kotlinx.android.synthetic.main.activity_choose_profile_pick.*
import kotlinx.android.synthetic.main.activity_gallery_photo.*
import kotlinx.android.synthetic.main.activity_gallery_photo.btn_continue
import kotlinx.android.synthetic.main.activity_gallery_photo.lyt_bar
import kotlinx.android.synthetic.main.activity_gallery_photo.lyt_parent
import kotlinx.android.synthetic.main.toolbar_main.*
import java.io.File


class GalleryPhotoActivity : AppCompatActivity(), View.OnClickListener,
    ImageSelectionHelper.ImageSelectionListener {

    private var mToolbarLayout: Toolbar? = null

    private var mImageCaptureHelper: ImageSelectionHelper? = null
    private var photoFile: File? = null
    private var photoFile_IMG_1: File? = null
    private var photoFile_IMG_2: File? = null
    private var photoFile_IMG_3: File? = null
    private var photoFile_IMG_4: File? = null
    private var photoFile_IMG_5: File? = null

    private var mGalleryImageFileList: HashMap<Int, File>? = null
    private var mGalleryImageDownloadUrlList: HashMap<Int, String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery_photo)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        tv_toolbarTitle.text = getString(R.string.title_gallery_photo)
        mImageCaptureHelper = ImageSelectionHelper(this@GalleryPhotoActivity, this)
        mGalleryImageFileList = HashMap<Int, File>()
        mGalleryImageDownloadUrlList = HashMap<Int, String>()
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)

        lyt_image1.setOnClickListener(this)
        lyt_image2.setOnClickListener(this)
        lyt_image3.setOnClickListener(this)
        lyt_image4.setOnClickListener(this)
        lyt_image5.setOnClickListener(this)

        btn_continue.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.lyt_image1 -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.GALLERY_IMAGE_1, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.lyt_image2 -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.GALLERY_IMAGE_2, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.lyt_image3 -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.GALLERY_IMAGE_3, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.lyt_image4 -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.GALLERY_IMAGE_4, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.lyt_image5 -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.GALLERY_IMAGE_5, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.btn_continue -> {
                if (NetworkUtils.isNetworkAvailable(this@GalleryPhotoActivity)) {
                    if (isValidate()) {
                        submitGalleryImages()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (photoFile == null) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_gallery))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun submitGalleryImages() {
        if (!mGalleryImageFileList.isNullOrEmpty()) {
            lyt_bar.visibility = View.VISIBLE
            val storageReference = FirebaseStorage.getInstance().reference
            val imageUriHashMapIterator: Iterator<*> = mGalleryImageFileList?.keys?.iterator()!!
            while (imageUriHashMapIterator.hasNext()) {
                val key = imageUriHashMapIterator.next() as Int
                val imageFileValue = mGalleryImageFileList?.get(key)
                val fileUri = Uri.fromFile(imageFileValue)

                val filepath = storageReference
                    .child("galleryImage/")
                    .child(Prefs.getString(AppConstants.USER_ID) + "/")
                    .child(imageFileValue?.name!!)

                filepath.putFile(fileUri!!)
                    .addOnSuccessListener(
                        OnSuccessListener<UploadTask.TaskSnapshot?> {
                            filepath.getDownloadUrl().addOnSuccessListener(
                                OnSuccessListener<Uri> { uri ->
                                    mGalleryImageDownloadUrlList?.put(key, uri.toString())
                                    if (mGalleryImageDownloadUrlList?.size!!.equals(mGalleryImageFileList?.size)) {
                                        updateGalleryRecords()
                                    }
                                }
                            )
                        }
                    )
                    .addOnFailureListener { exception ->
                        lyt_bar.visibility = View.GONE
                        ToastMsgUtils.showErrorMsg(lyt_parent, exception.message!!)
                    }
            }
        } else {
            ToastMsgUtils.showErrorMsg(lyt_parent, "No image added into list")
        }
    }

    private fun updateGalleryRecords() {
        if (!mGalleryImageDownloadUrlList.isNullOrEmpty()) {
            val db = FirebaseFirestore.getInstance()
            val imageDownloadUrlHashMapIterator: Iterator<*> = mGalleryImageDownloadUrlList?.keys?.iterator()!!

            val updateGalleryPhotoUrl: MutableMap<String, Any> = HashMap()
            while (imageDownloadUrlHashMapIterator.hasNext())
            {
                val key = imageDownloadUrlHashMapIterator.next() as Int
                val urlValue = mGalleryImageDownloadUrlList?.get(key)
                when(key)
                {
                    AppConstants.GALLERY_IMAGE_1 ->
                    {
                        updateGalleryPhotoUrl["gallery_image0"] = urlValue!!
                    }
                    AppConstants.GALLERY_IMAGE_2 ->
                    {
                        updateGalleryPhotoUrl["gallery_image1"] = urlValue!!
                    }
                    AppConstants.GALLERY_IMAGE_3 ->
                    {
                        updateGalleryPhotoUrl["gallery_image2"] = urlValue!!
                    }
                    AppConstants.GALLERY_IMAGE_4 ->
                    {
                        updateGalleryPhotoUrl["gallery_image3"] = urlValue!!
                    }
                    AppConstants.GALLERY_IMAGE_5 ->
                    {
                        updateGalleryPhotoUrl["gallery_image4"] = urlValue!!
                    }
                }
            }

            db.collection("users").document(Prefs.getString(AppConstants.USER_ID))
                .update(updateGalleryPhotoUrl)
                .addOnSuccessListener {
                    lyt_bar.visibility = View.GONE
                    goToNextActivity()
                }
                .addOnFailureListener {
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(
                        lyt_parent,
                        "" + it.message
                    )
                }
        } else {
            ToastMsgUtils.showErrorMsg(lyt_parent, "No image downloaded path")
        }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@GalleryPhotoActivity, FavoriteArtistActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    override fun capturedImage(uri: Uri?, imageFile: File?, requestTypeCode: Int, mFileSelectionType: Int) {
        if (imageFile != null) {
            photoFile = imageFile
            when (requestTypeCode) {
                AppConstants.GALLERY_IMAGE_1 -> {
                    photoFile_IMG_1 = imageFile
                    mGalleryImageFileList?.put(AppConstants.GALLERY_IMAGE_1, photoFile_IMG_1!!)

                    iv_galleryImage1.visibility = View.VISIBLE
                    iv_cameraImage1.visibility = View.GONE
                    Glide.with(this@GalleryPhotoActivity)
                        .load(photoFile_IMG_1)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .into(iv_galleryImage1)
                }
                AppConstants.GALLERY_IMAGE_2 -> {
                    photoFile_IMG_2 = imageFile
                    mGalleryImageFileList?.put(AppConstants.GALLERY_IMAGE_2, photoFile_IMG_2!!)

                    iv_galleryImage2.visibility = View.VISIBLE
                    iv_cameraImage2.visibility = View.GONE
                    Glide.with(this@GalleryPhotoActivity)
                        .load(photoFile_IMG_2)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .into(iv_galleryImage2)
                }
                AppConstants.GALLERY_IMAGE_3 -> {
                    photoFile_IMG_3 = imageFile
                    mGalleryImageFileList?.put(AppConstants.GALLERY_IMAGE_3, photoFile_IMG_3!!)

                    iv_galleryImage3.visibility = View.VISIBLE
                    iv_cameraImage3.visibility = View.GONE
                    Glide.with(this@GalleryPhotoActivity)
                        .load(photoFile_IMG_3)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .into(iv_galleryImage3)
                }
                AppConstants.GALLERY_IMAGE_4 -> {
                    photoFile_IMG_4 = imageFile
                    mGalleryImageFileList?.put(AppConstants.GALLERY_IMAGE_4, photoFile_IMG_4!!)

                    iv_galleryImage4.visibility = View.VISIBLE
                    iv_cameraImage4.visibility = View.GONE
                    Glide.with(this@GalleryPhotoActivity)
                        .load(photoFile_IMG_4)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .into(iv_galleryImage4)
                }
                AppConstants.GALLERY_IMAGE_5 -> {
                    photoFile_IMG_5 = imageFile
                    mGalleryImageFileList?.put(AppConstants.GALLERY_IMAGE_5, photoFile_IMG_5!!)

                    iv_galleryImage5.visibility = View.VISIBLE
                    iv_cameraImage5.visibility = View.GONE
                    Glide.with(this@GalleryPhotoActivity)
                        .load(photoFile_IMG_5)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .into(iv_galleryImage5)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mImageCaptureHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mImageCaptureHelper?.onActivityResult(requestCode, resultCode, data)
    }
}