package com.muzified.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.dialog.DialogUtils
import com.muzified.model.SignUpModel
import kotlinx.android.synthetic.main.activity_bio_seeking.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.et_email
import kotlinx.android.synthetic.main.activity_login.et_password
import kotlinx.android.synthetic.main.activity_login.lyt_bar
import kotlinx.android.synthetic.main.activity_login.lyt_parent
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.toolbar_main.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.util.*
import java.util.regex.Pattern


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    val db = Firebase.firestore
    private var mAuth: FirebaseAuth? = null
    private var mToolbarLayout: Toolbar? = null
    private var mCallbackManager: CallbackManager? = null
    private var mSignUpModel: SignUpModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        mAuth = FirebaseAuth.getInstance()
        mSignUpModel = SignUpModel()
        mCallbackManager = CallbackManager.Factory.create()

        val spannable1 = SpannableString("Login to ")
        spannable1.setSpan(
            ForegroundColorSpan(Color.parseColor("#ffffff")),
            0,
            "Login to ".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_loginTitle.text = spannable1
        val spannable2 = SpannableString("Muzified")
        spannable2.setSpan(
            ForegroundColorSpan(Color.parseColor("#E53935")),
            0,
            "Muzified".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_loginTitle.append(spannable2)
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
        tv_login.setOnClickListener(this)
        tv_forgotPassword.setOnClickListener(this)
        tv_loginWithFaceBook.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.tv_login -> {
                Utility.hideKeyboard(this, tv_login)
                if (NetworkUtils.isNetworkAvailable(this@LoginActivity)) {
                    if (isValidate()) {
                        initiateLoginProcess()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
            R.id.tv_loginWithFaceBook -> {
                Utility.hideKeyboard(this, tv_login)
                if (NetworkUtils.isNetworkAvailable(this@LoginActivity)) {
                    faceBookLogin()
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
            R.id.tv_forgotPassword -> {
                val intent = Intent(this@LoginActivity, ForgotPassword::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }
    }

    private fun faceBookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
            this,
            Arrays.asList(
                "public_profile", "email"
            )
        )
        LoginManager.getInstance().registerCallback(mCallbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(result: LoginResult?) {
                    Log.d("TAG", "facebook:onSuccess:$result")
                    //handleFacebookAccessToken(result?.accessToken!!)

                    val request = GraphRequest.newMeRequest(result?.accessToken,
                        object : GraphRequest.GraphJSONObjectCallback {
                            override fun onCompleted(
                                `object`: JSONObject?,
                                response: GraphResponse?
                            ) {
                                try {
                                    if (response != null) {
                                        val id = `object`?.getString("id")
                                        val first_name = `object`?.getString("first_name")
                                        val last_name = `object`?.getString("last_name")
                                        var gender = ""
                                        if (`object`?.has("email")!!) {
                                            gender = `object`.getString("gender")
                                        }

                                        var profile_pic: String = ""
                                        try {
                                            profile_pic =
                                                URL("https://graph.facebook.com/$id/picture?type=large").toString()
                                            Log.i("profile_pic", profile_pic.toString() + "")
                                        } catch (e: MalformedURLException) {
                                            e.printStackTrace()
                                        }
                                        var email = ""
                                        if (`object`.has("email")) {
                                            email = `object`.getString("email")
                                        }

                                        mSignUpModel?.first_name = first_name!!
                                        mSignUpModel?.last_name = last_name!!
                                        mSignUpModel?.gender = gender
                                        mSignUpModel?.profile = profile_pic
                                        mSignUpModel?.email = email
                                        mSignUpModel?.isProfileCompleted = false
                                        handleFacebookAccessToken(result?.accessToken!!)
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                } catch (e: IOException) {
                                    e.printStackTrace()
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        })

                    val parameters = Bundle()
                    parameters.putString("fields", "id,first_name,last_name,email,gender")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    Log.d("TAG", "facebook:onCancel")
                }

                override fun onError(error: FacebookException?) {
                    Log.d("TAG", "facebook:onError", error)
                    deleteAccessToken();
                }
            })
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_email))
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString().trim()).matches()) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_invalid_email))
        } else if (TextUtils.isEmpty(et_password.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_password))
        } else if (et_password.text.toString().trim().length < 6) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_invalid_password))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun initiateLoginProcess() {
        lyt_bar.visibility = View.VISIBLE
        mAuth?.signInWithEmailAndPassword(
            et_email.text.toString().trim(),
            et_password.text.toString().trim()
        )
            ?.addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) {
                    Prefs.putString(AppConstants.USER_ID, task.getResult()?.getUser()?.uid!!)
                    checkUserAlreadyExistInUserCollection()
                    //goToMainActivity()
                } else {
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "" + task.exception?.message)
                }
            }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        lyt_bar.visibility = View.VISIBLE
        Log.d("TAG", "handleFacebookAccessToken:$token")
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth?.signInWithCredential(credential)!!
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = mAuth?.currentUser
                    Prefs.putString(AppConstants.USER_ID, user?.uid!!)
                    checkUserAlreadyExistInUserCollection()
                } else {
                    // If sign in fails, display a message to the user.
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "" + task.exception?.message)
                }
            }
    }

    private fun checkUserAlreadyExistInUserCollection() {
        db.collection("users")
            .whereEqualTo("user_id", Prefs.getString(AppConstants.USER_ID))
            .get()
            .addOnSuccessListener { result ->
                if (!result.isEmpty) {
                    val userModel = SignUpModel()
                    for (document in result) {
                        val userData = document.data
                        if (userData.contains("user_id") && !TextUtils.isEmpty(userData.get("user_id") as String)) {
                            userModel.user_id = userData.get("user_id") as String
                        }
                        if (userData.contains("isProfileCompleted") && userData.get("isProfileCompleted") != null) {
                            userModel.isProfileCompleted =
                                userData.get("isProfileCompleted") as Boolean
                        }
                        if (userData.contains("first_name") && !TextUtils.isEmpty(userData.get("first_name") as String)) {
                            userModel.first_name = userData.get("first_name") as String
                        }
                        if (userData.contains("last_name") && !TextUtils.isEmpty(userData.get("last_name") as String)) {
                            userModel.last_name = userData.get("last_name") as String
                        }
                        if (userData.contains("dob") && !TextUtils.isEmpty(userData.get("dob").toString()))
                        {
                            val dateStr = userData.get("dob").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.dob = userData.get("dob") as Long
                            }

                        }
                        if (userData.contains("email") && !TextUtils.isEmpty(userData.get("email") as String)) {
                            userModel.email = userData.get("email") as String
                        }
                        if (userData.contains("gender") && !TextUtils.isEmpty(userData.get("gender") as String)) {
                            userModel.gender = userData.get("gender") as String
                        }
                        if (userData.contains("profile") && !TextUtils.isEmpty(userData.get("profile") as String)) {
                            userModel.profile = userData.get("profile") as String
                        }
                        if (userData.contains("bio") && !TextUtils.isEmpty(userData.get("bio") as String)) {
                            userModel.bio = userData.get("bio") as String
                        }
                        if (userData.contains("lat") && !TextUtils.isEmpty(userData.get("lat") as String)) {
                            userModel.lat = userData.get("lat") as String
                        }
                        if (userData.contains("long") && !TextUtils.isEmpty(userData.get("long") as String)) {
                            userModel.long = userData.get("long") as String
                        }
                        if (userData.contains("gender_preference") && !TextUtils.isEmpty(
                                userData.get(
                                    "gender_preference"
                                ) as String
                            )
                        ) {
                            userModel.gender_preference =
                                userData.get("gender_preference") as String
                        }
                        if (userData.contains("gallery_image0") && !TextUtils.isEmpty(userData.get("gallery_image0") as String)) {
                            userModel.gallery_image0 = userData.get("gallery_image0") as String
                        }
                        if (userData.contains("gallery_image1") && !TextUtils.isEmpty(userData.get("gallery_image1") as String)) {
                            userModel.gallery_image1 = userData.get("gallery_image1") as String
                        }
                        if (userData.contains("gallery_image2") && !TextUtils.isEmpty(userData.get("gallery_image2") as String)) {
                            userModel.gallery_image2 = userData.get("gallery_image2") as String
                        }
                        if (userData.contains("gallery_image3") && !TextUtils.isEmpty(userData.get("gallery_image3") as String)) {
                            userModel.gallery_image3 = userData.get("gallery_image3") as String
                        }
                        if (userData.contains("gallery_image4") && !TextUtils.isEmpty(userData.get("gallery_image4") as String)) {
                            userModel.gallery_image4 = userData.get("gallery_image4") as String
                        }
//                        if (userData.contains("date_created") && userData.get("date_created") != null) {
//                            userModel.date_created = userData.get("date_created") as Long
//                        }
                        if (userData.contains("date_created") && !TextUtils.isEmpty(userData.get("date_created").toString()))
                        {
                            val dateStr = userData.get("date_created").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
//                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
//                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.date_created = userData.get("date_created") as Long
                            }

                        }
                    }
                    lyt_bar.visibility = View.GONE
                    if (TextUtils.isEmpty(userModel.first_name) || TextUtils.isEmpty(userModel.last_name) ||
                        TextUtils.isEmpty(userModel.email) || TextUtils.isEmpty(userModel.gender)
                    ) {
                        userModel.first_name = mSignUpModel?.first_name!!
                        userModel.last_name = mSignUpModel?.last_name!!
                        if (!TextUtils.isEmpty(mSignUpModel?.email)) {
                            userModel.email = mSignUpModel?.email!!
                        }
                        Prefs.putString(AppConstants.LOGIN_TYPE, AppConstants.FACEBOOK_TYPE)
                        DialogUtils.showAlertDetailDialog(this@LoginActivity, userModel, AppConstants.SIGN_UP_SCREEN)
                    } else if (TextUtils.isEmpty(userModel.profile)) {
                        if (!TextUtils.isEmpty(mSignUpModel?.profile)) {
                            userModel.profile = mSignUpModel?.profile!!
                        }
                        Prefs.putString(AppConstants.LOGIN_TYPE, AppConstants.FACEBOOK_TYPE)
                        DialogUtils.showAlertDetailDialog(this@LoginActivity, userModel, AppConstants.PROFILE_PICK_SELECTION_SCREEN)
                    } else if (TextUtils.isEmpty(userModel.bio)) {
                        DialogUtils.showAlertDetailDialog(this@LoginActivity, userModel, AppConstants.BIO_SEEKING_SCREEN)
                    } else if (TextUtils.isEmpty(userModel.gallery_image0) &&
                        TextUtils.isEmpty(userModel.gallery_image1) &&
                        TextUtils.isEmpty(userModel.gallery_image2) &&
                        TextUtils.isEmpty(userModel.gallery_image3) &&
                        TextUtils.isEmpty(userModel.gallery_image4)) {
                        Prefs.putString(AppConstants.LOGIN_TYPE, AppConstants.FACEBOOK_TYPE)
                        DialogUtils.showAlertDetailDialog(this@LoginActivity, userModel, AppConstants.GALLERY_IMAGE_SCREEN)
                    } else if (TextUtils.isEmpty(userModel.lat) && TextUtils.isEmpty(userModel.long))
                    {
                        updateLatLong()
                    } else {
                        goToMainActivity()
                    }
                } else {
                    lyt_bar.visibility = View.GONE
                    Prefs.putString(AppConstants.LOGIN_TYPE, AppConstants.FACEBOOK_TYPE)
                    DialogUtils.showAlertDetailDialog(this@LoginActivity, mSignUpModel!!, AppConstants.SIGN_UP_SCREEN)
                }
            }
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, "" + exception.message)
            }
    }

    private fun updateLatLong()
    {
        val db = FirebaseFirestore.getInstance()
        val updateData: MutableMap<String, Any> = HashMap()
        updateData["lat"] = Prefs.getString(AppConstants.USER_LATITUDE)
        updateData["long"] = Prefs.getString(AppConstants.USER_LONGITUDE)
        val usersRef = db.collection("users")
        val id =
        usersRef.document(Prefs.getString(AppConstants.USER_ID))
            .update(updateData)
            .addOnSuccessListener {
                goToMainActivity()
            }
            .addOnFailureListener {

            }
    }

    private fun goToMainActivity() {
        val signUpIntent = Intent(this@LoginActivity, ConfirmProfileActivity::class.java)
        signUpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(signUpIntent)
        finish()
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


//    override fun onStart() {
//        super.onStart()
//        val currentUser = mAuth?.currentUser
//        if (currentUser != null) {
//            Prefs.putString(AppConstants.LOGIN_TYPE, AppConstants.EMAIL_TYPE)
//            checkUserAlreadyExistInUserCollection(currentUser)
//        }
//    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        mCallbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    private fun deleteAccessToken() {
        val accessTokenTracker: AccessTokenTracker = object : AccessTokenTracker() {
            override fun onCurrentAccessTokenChanged(
                oldAccessToken: AccessToken,
                currentAccessToken: AccessToken
            ) {
                if (currentAccessToken == null) {
                    //User logged out
                    LoginManager.getInstance().logOut()
                }
            }
        }
    }
}
