package com.muzified.activity

import android.os.Bundle
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.model.SignUpModel
import com.potyvideo.library.globalInterfaces.AndExoPlayerListener
import kotlinx.android.synthetic.main.toolbar_main.*
import kotlinx.android.synthetic.main.activity_video_play.*

class VideoPlayActivity: AppCompatActivity() , View.OnClickListener,
    AndExoPlayerListener {

    private var mediaController: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_play)

        setupUI()
        setupClickListener()
    }

    private fun setupUI()
    {
        mediaController = MediaController(this@VideoPlayActivity)

        if (intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_USER_DATA) != null)
        {
            val mSelectedUserData = intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_USER_DATA) as SignUpModel
            tv_toolbarTitle.text = mSelectedUserData?.first_name + " " + mSelectedUserData?.last_name
        }

        if (intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_MSG) != null)
        {
            val mSelectedVideoLink = intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_MSG) as String
            andExoPlayerView.setSource(mSelectedVideoLink)
            andExoPlayerView.setAndExoPlayerListener(this)

//            videoView.setVideoPath(mSelectedVideoLink)
//            lyt_progressBar.setVisibility(View.VISIBLE)
//            mediaController?.setMediaPlayer(videoView)
//            videoView.setMediaController(mediaController)
//            videoView.requestFocus()
//            videoView.start()
//
//            videoView.setOnPreparedListener { mp ->
//                mp.start()
//                mp.setOnVideoSizeChangedListener { mp, arg1, arg2 -> // TODO Auto-generated method stub
//                    lyt_progressBar.visibility = View.GONE
//                    mp.start()
//                }
//            }
        }
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->{
                releaseVideoPlayer()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        releaseVideoPlayer()
    }

    private fun releaseVideoPlayer()
    {
        if (andExoPlayerView.playerView.player?.isPlaying!!)
        {
            andExoPlayerView.stopPlayer()
            andExoPlayerView.releasePlayer()
        }

//        if (videoView.isPlaying)
//        {
//            videoView.stopPlayback()
//        }
        finish()
        //SlideAnimationUtil.slideBackAnimation(this@VideoPlayActivity)
    }
}