package com.muzified.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.NetworkUtils
import com.muzified.Utils.Prefs
import com.muzified.Utils.ToastMsgUtils
import com.muzified.adapter.FavoriteArtistAdapter
import com.muzified.adapter.FavoriteMusicAdapter
import com.muzified.model.FavoriteMusicModel
import kotlinx.android.synthetic.main.activity_chat_request.*
import kotlinx.android.synthetic.main.activity_favorite_music.*
import kotlinx.android.synthetic.main.activity_favorite_music.*
import kotlinx.android.synthetic.main.activity_favorite_music.et_searchView
import kotlinx.android.synthetic.main.activity_favorite_music.iv_clear
import kotlinx.android.synthetic.main.activity_favorite_music.lyt_bar
import kotlinx.android.synthetic.main.activity_favorite_music.lyt_parent
import kotlinx.android.synthetic.main.toolbar_favorite_artist.*

class FavoriteMusicActivity: AppCompatActivity(), View.OnClickListener {

    private var mToolbarLayout: Toolbar? = null
    private var mFavoriteMusicArrayList: ArrayList<FavoriteMusicModel>? = null
    private var mAdapter: FavoriteMusicAdapter? = null

    private val musicName = arrayOf(
        "Rock", "Alternative", "Classic Rock", "Classical", "Jazz", "Blues", "Classical Rock/Progressive", "Oldies", "Progressive Rock", "Progressive Rock", "Rap/hip-hop", "Dance/Electronica", "Folk", "Indie Rock", "Soul/funk", "Soundtracks/theme Songs", "Thrash Metal", "Grunge", "Country", "Muzziac added Púnk Rock", "Axelanti added Reggae"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_music)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupArtistRecyclerview()
        setupClickListener()
    }

    private fun setupUI()
    {
        tv_toolbarTitle.text = getString(R.string.title_favorite_music)
        tv_toolbarSubTitle.text = getString(R.string.sub_title_favorite_music)
    }

    private fun setupArtistRecyclerview()
    {
        mFavoriteMusicArrayList = ArrayList()
        mFavoriteMusicArrayList = AppConstants.getFavouriteMusicList()

        mAdapter = FavoriteMusicAdapter(this@FavoriteMusicActivity, mFavoriteMusicArrayList)
        val mLayoutManager = LinearLayoutManager(this@FavoriteMusicActivity)
        rv_favoriteMusic.layoutManager = mLayoutManager
        rv_favoriteMusic.adapter = mAdapter
        rv_favoriteMusic.hasFixedSize()

        et_searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable?.length!! > 0)
                {
                    iv_clear.visibility = View.VISIBLE
                }
                else
                {
                    iv_clear.visibility = View.GONE
                }
                mAdapter?.filter?.filter(editable)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
        btn_artist_selected.setOnClickListener(this)
        iv_clear?.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.btn_artist_selected ->
            {
                if (NetworkUtils.isNetworkAvailable(this@FavoriteMusicActivity)) {
                    if (isValidate()) {
                        submitFavoriteArtistResult()
                    }
                    else
                    {
                        ToastMsgUtils.showErrorMsg(
                            lyt_parent,
                            getString(R.string.error_msg_favorite_music_not_selected)
                        )
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
            R.id.iv_clear ->
            {
                et_searchView.setText("")
                et_searchView.hideKeyboard()
                iv_clear.visibility = View.GONE
                mAdapter?.filter?.filter("")
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun isValidate(): Boolean {
        var isValid = false
        for (i in 0 until mFavoriteMusicArrayList?.size!!) {
            val music = mFavoriteMusicArrayList?.get(i)
            if (music?.isSelected!!) {
                isValid = true
                break
            } else {
                isValid = false
            }
        }
        return isValid
    }

    private fun getFavoriteMusicIds(): List<String> {
        val musicId = ArrayList<String>()
        for (i in 0 until mFavoriteMusicArrayList?.size!!) {
            val music = mFavoriteMusicArrayList?.get(i)
            if (music?.isSelected!!) {
                musicId.add(music.id)
            }
        }
        return musicId
    }

    private fun submitFavoriteArtistResult() {
        lyt_bar.visibility = View.VISIBLE
        val selectedMusicId = getFavoriteMusicIds()
        val updateBio: MutableMap<String, Any> = HashMap()
        updateBio["favourites_music_ids"] = selectedMusicId
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(Prefs.getString(AppConstants.USER_ID))
            .update(updateBio)
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE
                goToNextActivity()
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(
                    lyt_parent,
                    "" + it.message
                )
            }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@FavoriteMusicActivity, ConfirmProfileActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}