package com.muzified.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.muzified.R
import com.muzified.Utils.*
import kotlinx.android.synthetic.main.activity_choose_profile_pick.*
import kotlinx.android.synthetic.main.toolbar_main.*
import java.io.File

class ProfilePickSelectionActivity : AppCompatActivity(), View.OnClickListener,
    ImageSelectionHelper.ImageSelectionListener {

    private var mToolbarLayout: Toolbar? = null
    private var mAuth: FirebaseAuth? = null
    private var mImageFile: File? = null
    private var mImageCaptureHelper: ImageSelectionHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_profile_pick)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        mAuth = FirebaseAuth.getInstance()
        tv_toolbarTitle.text = getString(R.string.title_profile_photo)
        mImageCaptureHelper = ImageSelectionHelper(this@ProfilePickSelectionActivity, this)
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
        btn_continue.setOnClickListener(this)
        lyt_profilePick.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.lyt_profilePick -> {
                mImageCaptureHelper?.openImageSelectionDialog(AppConstants.DEFAULT_IMAGE, "Choose image from", AppConstants.FILE_TYPE_IMAGE)
            }
            R.id.btn_continue -> {
                if (NetworkUtils.isNetworkAvailable(this@ProfilePickSelectionActivity)) {
                    if (isValidate()) {
                        uploadProfileImage()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (mImageFile == null) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_profile_pick))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun uploadProfileImage() {
        lyt_bar.visibility = View.VISIBLE
        val fileUri = Uri.fromFile(mImageFile)
        val storageReference = FirebaseStorage.getInstance().reference
        val filepath = storageReference
            .child("userPics/")
            .child(Prefs.getString(AppConstants.USER_ID) + "/")
            .child(mImageFile?.name!!)

        filepath.putFile(fileUri)
            .addOnSuccessListener(
                OnSuccessListener<UploadTask.TaskSnapshot?> {
                    filepath.getDownloadUrl().addOnSuccessListener(
                        OnSuccessListener<Uri> { uri ->
                            addUploadRecordToDb(uri)
                        }
                    )
                }
            )
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, exception.message!!)
            }
    }

    private fun addUploadRecordToDb(uri: Uri) {
        val db = FirebaseFirestore.getInstance()
        val updateUri: MutableMap<String, Any> = HashMap()
        updateUri["profile"] = uri.toString()
        db.collection("users").document(Prefs.getString(AppConstants.USER_ID))
            .update(updateUri)
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE
                goToNextActivity()
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(
                    lyt_parent,
                    "" + it.message
                )
            }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@ProfilePickSelectionActivity, BioSeekingActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mImageCaptureHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mImageCaptureHelper?.onActivityResult(requestCode, resultCode, data)
    }

    override fun capturedImage(uri: Uri?, imageFile: File?, viewIdCode: Int, mFileSelectionType: Int) {
        if (imageFile != null) {
            mImageFile = imageFile
            iv_profilePick.visibility = View.VISIBLE
            iv_profileCamera.visibility = View.GONE
            Glide.with(this@ProfilePickSelectionActivity)
                .load(mImageFile)
                .centerCrop()
                .placeholder(R.drawable.place_holder)
                .into(iv_profilePick)
        }
    }
}
