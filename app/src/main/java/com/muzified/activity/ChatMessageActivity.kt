package com.muzified.activity

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.TranslateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.*
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.adapter.ChatMessagesAdapter
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.conversation.ConversationResultModel
import com.muzified.model.conversation.MessageModel
import kotlinx.android.synthetic.main.activity_chat_list.*
import kotlinx.android.synthetic.main.toolbar_main.*
import kotlinx.android.synthetic.main.activity_chat_messages.*
import kotlinx.android.synthetic.main.activity_chat_messages.lyt_bar
import kotlinx.android.synthetic.main.activity_chat_messages.lyt_parent
import kotlinx.android.synthetic.main.activity_choose_profile_pick.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class ChatMessageActivity : AppCompatActivity(), View.OnClickListener,
    ItemClickListenerWithViewType, ImageSelectionHelper.ImageSelectionListener {

    private var rootRef: DatabaseReference? = null
    private var chatRef: DatabaseReference? = null
    val db = Firebase.firestore
    private var TAG = "ChatMessageActivity"
    private var mToolbarLayout: Toolbar? = null
    private var mChatMessagesArrayList: ArrayList<MessageModel>? = null
    private var mChatListAdapter: ChatMessagesAdapter? = null
    private var selectedConversationModel: ConversationResultModel? = null
    private var mImageFile: File? = null
    private var mImageCaptureHelper: ImageSelectionHelper? = null
    private var isOptionViewOpen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_messages)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
        getAllChatMessages()
        mImageCaptureHelper = ImageSelectionHelper(this@ChatMessageActivity, this)
    }

    private fun initialiseUI() {
        selectedConversationModel = intent.getSerializableExtra(AppConstants.SELECTED_CONVERSATION_ID) as ConversationResultModel

        tv_toolbarTitle.text = selectedConversationModel?.userProfileModel?.first_name + " " + selectedConversationModel?.userProfileModel?.last_name

        mChatMessagesArrayList = ArrayList<MessageModel>()
        mChatListAdapter =
            ChatMessagesAdapter(this, mChatMessagesArrayList!!, this)
        val mLayoutManager = LinearLayoutManager(this@ChatMessageActivity)
        rv_chatMessagesList.layoutManager = mLayoutManager
        rv_chatMessagesList.adapter = mChatListAdapter
        rv_chatMessagesList.hasFixedSize()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onBackPress()
    }

    private fun onBackPress() {
        finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
        iv_chatOption.setOnClickListener(this)
        iv_sendBtn.setOnClickListener(this)

        iv_selectImage.setOnClickListener(this)
        iv_selectVideo.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                onBackPress()
            }
            R.id.iv_chatOption -> {
                if (!isOptionViewOpen) {
                    iv_chatOption.rotation = 180f
                    lyt_optionView.visibility = View.VISIBLE
                    val animate = TranslateAnimation(
                        0f,
                        0f,
                        lyt_optionView.height.toFloat(),
                        0f
                    )
                    animate.duration = 500
                    animate.fillAfter = true
                    lyt_optionView.startAnimation(animate)
                } else {
                    iv_chatOption.rotation = 0f
                    lyt_optionView.visibility = View.GONE
                    val animate = TranslateAnimation(
                        0f,
                        0f,
                        0f,
                        lyt_optionView.height.toFloat()
                    )
                    animate.duration = 500
                    animate.fillAfter = true
                    lyt_optionView.startAnimation(animate)
                }
                isOptionViewOpen = !isOptionViewOpen
            }
            R.id.iv_selectImage -> {
                mImageCaptureHelper?.openImageSelectionDialog(
                    AppConstants.DEFAULT_IMAGE,
                    "Choose image from",
                    AppConstants.FILE_TYPE_IMAGE
                )
            }
            R.id.iv_selectVideo -> {
                mImageCaptureHelper?.openImageSelectionDialog(
                    AppConstants.DEFAULT_IMAGE,
                    "Choose video from",
                    AppConstants.FILE_TYPE_VIDEO
                )
            }
            R.id.iv_sendBtn -> {
                Utility.hideKeyboard(this@ChatMessageActivity, et_typeMessage!!)
                if (isValidMessage()) {
                    if (NetworkUtils.isNetworkAvailable(this@ChatMessageActivity)) {
                        sendMessage()
                    } else {
                        ToastMsgUtils.showSuccessMsg(
                            lyt_parent,
                            getString(R.string.error_msg_no_network)
                        )
                    }
                }
            }
        }
    }

    override fun onItemClick(position: Int, view: View) {
        val selectedMsg = mChatMessagesArrayList?.get(position)
        when (view.id) {
            R.id.lyt_imageMsgType -> {
                val chatMessageIntent = Intent(this@ChatMessageActivity, ViewFullImageActivity::class.java)
                chatMessageIntent.putExtra(AppConstants.SELECTED_CONVERSATION_MSG, selectedMsg?.profilePicLink)
                chatMessageIntent.putExtra(AppConstants.SELECTED_CONVERSATION_USER_DATA, selectedConversationModel?.userProfileModel)
                startActivity(chatMessageIntent)
            }
            R.id.iv_playVideo -> {
                val chatVideoIntent =
                    Intent(this@ChatMessageActivity, VideoPlayActivity::class.java)
                chatVideoIntent.putExtra(
                    AppConstants.SELECTED_CONVERSATION_MSG,
                    selectedMsg?.videoLink
                )
                chatVideoIntent.putExtra(
                    AppConstants.SELECTED_CONVERSATION_USER_DATA,
                    selectedConversationModel?.userProfileModel
                )
                startActivity(chatVideoIntent)
            }
        }
    }

    private fun getAllChatMessages() {
        if (NetworkUtils.isNetworkAvailable(this@ChatMessageActivity)) {
            fetchRecord()
        } else {
            ToastMsgUtils.showSuccessMsg(lyt_parent, getString(R.string.error_msg_no_network))
        }
    }

    private fun fetchRecord() {
        lyt_bar?.visibility = View.VISIBLE
        db.collection("Conversations").document(selectedConversationModel?.id!!)
            .collection("Messages")
            .addSnapshotListener { snapshots, e ->
                if (e != null) {
                    return@addSnapshotListener
                }

                if (snapshots?.documents?.isEmpty()!!) {
                    lyt_bar?.visibility = View.GONE
                }

                for (data in snapshots.documentChanges) {
                    when (data.type) {
                        DocumentChange.Type.ADDED -> {
                            updateResult(data, "ADDED")
                        }
                        DocumentChange.Type.MODIFIED -> {
                            updateResult(data, "MODIFIED")
                            Log.d(TAG, "Modified city: ${data.document.data}")
                        }
                        DocumentChange.Type.REMOVED -> {
                            updateResult(data, "REMOVED")
                            Log.d(TAG, "Removed city: ${data.document.data}")
                        }
                    }
                }

                lyt_bar?.visibility = View.GONE
                if (!mChatMessagesArrayList?.isNullOrEmpty()!!)
                {
                    for (i in 0 until mChatMessagesArrayList?.size!!)
                    {
                        val chatItem = mChatMessagesArrayList?.get(i)
                        if (!chatItem?.ownerID.equals(Prefs.getString(AppConstants.USER_ID)))
                        {
                            chatItem?.userProfileImage = selectedConversationModel?.userProfileModel?.profile!!
                        }
                    }
                    Collections.sort(mChatMessagesArrayList,
                        object : Comparator<MessageModel> {
                            override fun compare(p0: MessageModel?, p1: MessageModel?): Int {
                                return p0?.timestamp!!.compareTo(p1?.timestamp!!)
                            }
                        })
                    mChatListAdapter?.notifyDataSetChanged()
                    rv_chatMessagesList.scrollToPosition(mChatMessagesArrayList?.size!! - 1)
                }
            }
    }

    private fun updateResult(data: DocumentChange, s: String) {
        when (s) {
            "ADDED" -> {
                val chatModel = MessageModel()
                chatModel.timestamp = data.document.get("timestamp") as Long
                if (data.document.contains("id") && !TextUtils.isEmpty(
                        data.document.get(
                            "id"
                        ) as String
                    )
                ) {
                    chatModel.id = data.document.get("id") as String
                }
                if (data.document.contains("contentType")) {
                    chatModel.contentType = data.document.get("contentType") as Long
                }
                if (data.document.contains("ownerID") && !TextUtils.isEmpty(data.document.get("ownerID") as String)) {
                    chatModel.ownerID = data.document.get("ownerID") as String
                }
                if (data.document.contains("videoLink") && !TextUtils.isEmpty(data.document.get("videoLink") as String)) {
                    chatModel.videoLink = data.document.get("videoLink") as String
                }
                if (data.document.contains("profilePicLink") && !TextUtils.isEmpty(data.document.get("profilePicLink") as String)) {
                    chatModel.profilePicLink = data.document.get("profilePicLink") as String
                }
                if (data.document.contains("message") && !TextUtils.isEmpty(data.document.get("message") as String)) {
                    chatModel.message = data.document.get("message") as String
                }
                mChatMessagesArrayList?.add(chatModel)
            }
            "MODIFIED" ->{
                val chatModel = MessageModel()
                chatModel.timestamp = data.document.get("timestamp") as Long
                if (data.document.contains("id") && !TextUtils.isEmpty(
                        data.document.get(
                            "id"
                        ) as String
                    )
                ) {
                    chatModel.id = data.document.get("id") as String
                }
                if (data.document.contains("contentType")) {
                    chatModel.contentType = data.document.get("contentType") as Long
                }
                if (data.document.contains("ownerID") && !TextUtils.isEmpty(data.document.get("ownerID") as String)) {
                    chatModel.ownerID = data.document.get("ownerID") as String
                }
                if (data.document.contains("videoLink") && !TextUtils.isEmpty(data.document.get("videoLink") as String)) {
                    chatModel.videoLink = data.document.get("videoLink") as String
                }
                if (data.document.contains("profilePicLink") && !TextUtils.isEmpty(data.document.get("profilePicLink") as String)) {
                    chatModel.profilePicLink = data.document.get("profilePicLink") as String
                }
                if (data.document.contains("message") && !TextUtils.isEmpty(data.document.get("message") as String)) {
                    chatModel.message = data.document.get("message") as String
                }

                for (i in 0 until mChatMessagesArrayList?.size!!)
                {
                    val item = mChatMessagesArrayList?.get(i)
                    if (item?.id == chatModel.id)
                    {
                        item.contentType = chatModel.contentType
                        item.id = chatModel.id
                        item.message = chatModel.message
                        item.ownerID = chatModel.ownerID
                        item.profilePicLink = chatModel.profilePicLink
                        item.videoLink = chatModel.videoLink
                        item.videoLink = chatModel.videoLink
                        mChatListAdapter?.notifyItemChanged(i)
                        break
                    }
                }
            }
            "REMOVED" -> {
                val id = data.document.get("id") as String
                for (i in 0 until mChatMessagesArrayList?.size!!) {
                    val item = mChatMessagesArrayList?.get(i)
                    if (item?.id.equals(id)) {
                        mChatMessagesArrayList?.removeAt(i)
                        mChatListAdapter?.notifyItemChanged(i)
                        break
                    }
                }
            }
        }
    }

    private fun isValidMessage(): Boolean {
        var isValid: Boolean = false
        if (TextUtils.isEmpty(et_typeMessage?.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(lyt_parent, "Please type some message")
        } else {
            isValid = true
        }
        return isValid
    }

    private fun sendMessage() {
        lyt_bar.visibility = View.VISIBLE
        val addMessage: MutableMap<String, Any> = HashMap()
        val uuid = UUID.randomUUID().toString()
        val timestamp = Calendar.getInstance().timeInMillis
        addMessage["id"] = uuid
        addMessage["contentType"] = AppConstants.MSG_TYPE_TEXT.toLong()
        addMessage["ownerID"] = Prefs.getString(AppConstants.USER_ID)
        addMessage["timestamp"] = timestamp
        addMessage["message"] = et_typeMessage?.text.toString().trim()

        try {
            val reference = db.collection("Conversations").document(selectedConversationModel?.id!!)
                .collection("Messages")
            reference.document(uuid).set(addMessage)
                .addOnSuccessListener { documentReference ->
                    val messageModel = MessageModel()
                    messageModel.id = uuid
                    messageModel.contentType = AppConstants.MSG_TYPE_TEXT.toLong()
                    messageModel.ownerID = Prefs.getString(AppConstants.USER_ID)
                    messageModel.timestamp = timestamp
                    messageModel.message = et_typeMessage?.text.toString().trim()
                    updateConversationLastMsg(
                        AppConstants.MSG_TYPE_TEXT,
                        et_typeMessage?.text.toString().trim(),
                        timestamp,
                        messageModel
                    )
                    et_typeMessage?.setText("")
                }
                .addOnFailureListener { e ->
                    lyt_bar.visibility = View.GONE
                    Log.e("Failure", "Error adding document", e)
                }
        } catch (e: Exception) {
            lyt_bar.visibility = View.GONE
            Log.w("Exception", "Exception adding document", e)
        }
    }

    private fun updateConversationLastMsg(
        msgType: Int,
        textMsg: String,
        timestamp: Long,
        messageModel: MessageModel
    ) {
        val updateLastMessage: MutableMap<String, Any> = HashMap()
        when (msgType) {
            AppConstants.MSG_TYPE_TEXT -> {
                updateLastMessage["lastMessage"] = textMsg
            }
            AppConstants.MSG_TYPE_IMAGE -> {
                updateLastMessage["lastMessage"] = "Attachment"
            }
            AppConstants.MSG_TYPE_VIDEO -> {
                updateLastMessage["lastMessage"] = "Attachment"
            }
        }
        updateLastMessage["timestamp"] = timestamp
        db.collection("Conversations").document(selectedConversationModel?.id!!)
            .update(updateLastMessage)
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE
//                mChatMessagesArrayList?.add(mChatMessagesArrayList?.size!!, messageModel)
//                mChatListAdapter?.notifyItemInserted(mChatMessagesArrayList?.size!! - 1)
//                rv_chatMessagesList.scrollToPosition(mChatMessagesArrayList?.size!! - 1)
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
                Log.w("Exception", "Exception adding document", it)
            }
    }

    override fun capturedImage(
        uri: Uri?,
        imageFile: File?,
        requestTypeCode: Int,
        mFileSelectionType: Int
    ) {
        if (imageFile != null) {
            mImageFile = imageFile
            if (NetworkUtils.isNetworkAvailable(this@ChatMessageActivity)) {
                when (mFileSelectionType) {
                    AppConstants.FILE_TYPE_IMAGE -> {
                        uploadImageToFireStoreDB()
                    }
                    AppConstants.FILE_TYPE_VIDEO -> {
                        uploadVideoToFireStoreDB()
                    }
                }

            } else {
                ToastMsgUtils.showSuccessMsg(lyt_parent!!, getString(R.string.error_msg_no_network))
            }
        }
    }

    private fun uploadVideoToFireStoreDB() {
        lyt_bar.visibility = View.VISIBLE
        val storageReference = FirebaseStorage.getInstance().reference
        val uuid = UUID.randomUUID().toString()
        val fileUri = Uri.fromFile(mImageFile)
        val filepath = storageReference
            .child("Messages/")
            .child(uuid + "/")
            .child(mImageFile?.name!!)

        filepath.putFile(fileUri!!)
            .addOnSuccessListener(
                OnSuccessListener<UploadTask.TaskSnapshot?> {
                    filepath.getDownloadUrl().addOnSuccessListener(
                        OnSuccessListener<Uri> { uri ->
                            val path = uri.toString()
                            postVideoTypeMsg(uuid, path!!)
                        }
                    )
                }
            )
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, exception.message!!)
            }
    }

    private fun uploadImageToFireStoreDB() {
        lyt_bar.visibility = View.VISIBLE
        val storageReference = FirebaseStorage.getInstance().reference
        val uuid = UUID.randomUUID().toString()
        val fileUri = Uri.fromFile(mImageFile)
        val filepath = storageReference
            .child("Messages/")
            .child(uuid + "/")
            .child(mImageFile?.name!!)

        filepath.putFile(fileUri)
            .addOnSuccessListener(
                OnSuccessListener<UploadTask.TaskSnapshot?> {
                    filepath.getDownloadUrl().addOnSuccessListener(
                        OnSuccessListener<Uri> { uri ->
                            val path = uri.toString()
                            postImageTypeMsg(uuid, path)
                        }
                    )
                }
            )
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, exception.message!!)
            }
    }

    private fun postImageTypeMsg(documentId: String, imageFilePath: String) {
        val addMessage: MutableMap<String, Any> = HashMap()
        val timestamp = Calendar.getInstance().timeInMillis
        addMessage["id"] = documentId
        addMessage["contentType"] = AppConstants.MSG_TYPE_IMAGE.toLong()
        addMessage["ownerID"] = Prefs.getString(AppConstants.USER_ID)
        addMessage["timestamp"] = timestamp
        addMessage["profilePicLink"] = imageFilePath

        try {
            val reference = db.collection("Conversations").document(selectedConversationModel?.id!!)
                .collection("Messages")
            reference.document(documentId).set(addMessage)
                .addOnSuccessListener { documentReference ->
                    val messageModel = MessageModel()
                    messageModel.id = documentId
                    messageModel.contentType = AppConstants.MSG_TYPE_IMAGE.toLong()
                    messageModel.ownerID = Prefs.getString(AppConstants.USER_ID)
                    messageModel.timestamp = timestamp
                    messageModel.profilePicLink = imageFilePath
                    updateConversationLastMsg(
                        AppConstants.MSG_TYPE_IMAGE,
                        "",
                        timestamp,
                        messageModel
                    )
                }
                .addOnFailureListener { e ->
                    lyt_bar.visibility = View.GONE
                    Log.e("Failure", "Error adding document", e)
                }
        } catch (e: Exception) {
            lyt_bar.visibility = View.GONE
            Log.w("Exception", "Exception adding document", e)
        }
    }

    private fun postVideoTypeMsg(documentId: String, imageFilePath: String) {
        val addMessage: MutableMap<String, Any> = HashMap()
        val timestamp = Calendar.getInstance().timeInMillis
        addMessage["id"] = documentId
        addMessage["contentType"] = AppConstants.MSG_TYPE_VIDEO.toLong()
        addMessage["ownerID"] = Prefs.getString(AppConstants.USER_ID)
        addMessage["timestamp"] = timestamp
        addMessage["videoLink"] = imageFilePath

        try {
            val reference = db.collection("Conversations").document(selectedConversationModel?.id!!)
                .collection("Messages")
            reference.document(documentId).set(addMessage)
                .addOnSuccessListener { documentReference ->
                    val messageModel = MessageModel()
                    messageModel.id = documentId
                    messageModel.contentType = AppConstants.MSG_TYPE_VIDEO.toLong()
                    messageModel.ownerID = Prefs.getString(AppConstants.USER_ID)
                    messageModel.timestamp = timestamp
                    messageModel.videoLink = imageFilePath
                    updateConversationLastMsg(
                        AppConstants.MSG_TYPE_VIDEO,
                        "",
                        timestamp,
                        messageModel
                    )
                }
                .addOnFailureListener { e ->
                    lyt_bar.visibility = View.GONE
                    Log.e("Failure", "Error adding document", e)
                }
        } catch (e: Exception) {
            lyt_bar.visibility = View.GONE
            Log.w("Exception", "Exception adding document", e)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mImageCaptureHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mImageCaptureHelper?.onActivityResult(requestCode, resultCode, data)
    }
}