package com.muzified.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.NetworkUtils
import com.muzified.Utils.Prefs
import com.muzified.Utils.ToastMsgUtils
import com.muzified.adapter.FavoriteArtistAdapter
import com.muzified.adapter.FavoriteMusicAdapter
import com.muzified.listener.ItemClickListener
import com.muzified.model.FavoriteArtistModel
import kotlinx.android.synthetic.main.activity_chat_request.*
import kotlinx.android.synthetic.main.activity_favorite_artist.*
import kotlinx.android.synthetic.main.activity_favorite_artist.et_searchView
import kotlinx.android.synthetic.main.activity_favorite_artist.iv_clear
import kotlinx.android.synthetic.main.activity_favorite_artist.lyt_bar
import kotlinx.android.synthetic.main.activity_favorite_artist.lyt_parent
import kotlinx.android.synthetic.main.toolbar_favorite_artist.*
import java.lang.reflect.Array

class FavoriteArtistActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {

    private var mToolbarLayout: Toolbar? = null
    private var mFavoriteArtistArrayList: ArrayList<FavoriteArtistModel>? = null
    private var mAdapter: FavoriteArtistAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_artist)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupArtistRecyclerview()
        setupClickListener()
    }

    private fun setupUI() {
        tv_toolbarTitle.text = getString(R.string.title_favorite_artist)
        tv_toolbarSubTitle.text = getString(R.string.sub_title_favorite_artist)
    }

    private fun setupArtistRecyclerview() {
        mFavoriteArtistArrayList = ArrayList()
        mFavoriteArtistArrayList = AppConstants.getArtistList()

        mAdapter =
            FavoriteArtistAdapter(this@FavoriteArtistActivity, mFavoriteArtistArrayList, this)
        val mLayoutManager = LinearLayoutManager(this@FavoriteArtistActivity)
        rv_favoriteArtist.layoutManager = mLayoutManager
        rv_favoriteArtist.adapter = mAdapter
        rv_favoriteArtist.hasFixedSize()

        et_searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable?.length!! > 0)
                {
                    iv_clear.visibility = View.VISIBLE
                }
                else
                {
                    iv_clear.visibility = View.GONE
                }
                mAdapter?.filter?.filter(editable)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
        btn_artist_selected.setOnClickListener(this)
        iv_clear?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.btn_artist_selected -> {
                if (NetworkUtils.isNetworkAvailable(this@FavoriteArtistActivity)) {
                    if (isValidate()) {
                        submitFavoriteArtistResult()
                    }
                    else
                    {
                        ToastMsgUtils.showErrorMsg(
                            lyt_parent,
                            getString(R.string.error_msg_favorite_artist_not_selected)
                        )
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
            R.id.iv_clear ->
            {
                et_searchView.setText("")
                et_searchView.hideKeyboard()
                iv_clear.visibility = View.GONE
                mAdapter?.filter?.filter("")
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun isValidate(): Boolean {
        var isValid = false
        for (i in 0 until mFavoriteArtistArrayList?.size!!) {
            val artist = mFavoriteArtistArrayList?.get(i)
            if (artist?.isSelected!!) {
                isValid = true
                break
            } else {
                isValid = false
            }
        }
        return isValid
    }

    private fun getFavoriteArtistIds(): List<String> {
        val artistId = ArrayList<String>()
        for (i in 0 until mFavoriteArtistArrayList?.size!!) {
            val artist = mFavoriteArtistArrayList?.get(i)
            if (artist?.isSelected!!) {
                artistId.add(artist.id)
            }
        }
        return artistId
    }

    private fun submitFavoriteArtistResult() {
        lyt_bar.visibility = View.VISIBLE
        val selectedArtistId = getFavoriteArtistIds()
        val updateBio: MutableMap<String, Any> = HashMap()
        updateBio["favourites_artist_ids"] = selectedArtistId
        val db = FirebaseFirestore.getInstance()
        db.collection("users").document(Prefs.getString(AppConstants.USER_ID))
            .update(updateBio)
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE
                goToNextActivity()
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(
                    lyt_parent,
                    "" + it.message
                )
            }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@FavoriteArtistActivity, FavoriteMusicActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    override fun onItemClick(position: Int) {
    }
}