package com.muzified.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.adapter.GenderSelectionAdapter
import com.muzified.model.SignUpModel
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.toolbar_main.*
import java.util.*
import kotlin.collections.ArrayList


class SignUpActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private var mAuth: FirebaseAuth? = null
    private var mToolbarLayout: Toolbar? = null
    private var mSelectedGender: String = ""
    private var mSignUpModel: SignUpModel? = null
    private var mSelectedDateOfBirth: Long? = 0
    private var loginType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        if (intent.getSerializableExtra(AppConstants.SIGN_UP_USER_DATA) != null) {
            mSignUpModel =
                intent.getSerializableExtra(AppConstants.SIGN_UP_USER_DATA) as SignUpModel
        }
        loginType = Prefs.getString(AppConstants.LOGIN_TYPE)
        mAuth = FirebaseAuth.getInstance()
        val spannable1 = SpannableString("Sign up to ")
        spannable1.setSpan(
            ForegroundColorSpan(Color.parseColor("#ffffff")),
            0,
            "Sign up to ".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        tv_signUpTitle.text = spannable1
        val spannable2 = SpannableString("Muzified")
        spannable2.setSpan(
            ForegroundColorSpan(Color.parseColor("#E53935")),
            0,
            "Muzified".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_signUpTitle.append(spannable2)


        val iAgrreToSpannable = SpannableString("I agree to the ")
        iAgrreToSpannable.setSpan(
            ForegroundColorSpan(Color.parseColor("#ffffff")),
            0,
            "I agree to the ".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        tv_termsOfServicesText.text = iAgrreToSpannable

        val termsOfServiceSpannable = SpannableString("Terms of Service")
        termsOfServiceSpannable.setSpan(
            ForegroundColorSpan(Color.parseColor("#9d4aeb")),
            0,
            "Terms of Service".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_termsOfServicesText.append(termsOfServiceSpannable)

        val andSpannable = SpannableString(" and ")
        andSpannable.setSpan(
            ForegroundColorSpan(Color.parseColor("#ffffff")),
            0,
            " and ".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_termsOfServicesText.append(andSpannable)

        val privacyPolicySpannable = SpannableString("Privacy Policy")
        privacyPolicySpannable.setSpan(
            ForegroundColorSpan(Color.parseColor("#9d4aeb")),
            0,
            "Privacy Policy".length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        //I agree to the Terms of Service and Privacy Policy
        tv_termsOfServicesText.append(privacyPolicySpannable)
        val spannableString = SpannableString(tv_termsOfServicesText.text)
        spannableString.setSpan(termsAndCondition, 15, 31, 0)
        spannableString.setSpan(privacyPolicy, 36, 50, 0)
        tv_termsOfServicesText.setMovementMethod(LinkMovementMethod.getInstance())
        tv_termsOfServicesText.setText(spannableString, TextView.BufferType.SPANNABLE);
        tv_termsOfServicesText.setSelected(true)

        val genderList = ArrayList<String>()
        genderList.add("Gender")
        genderList.add("Male")
        genderList.add("Female")
        genderList.add("Other")
        val showroomSelectionAdapter = GenderSelectionAdapter(this@SignUpActivity, genderList)
        spinner_gender.adapter = showroomSelectionAdapter
        spinner_gender.onItemSelectedListener = this

        if (mSignUpModel != null) {
            if (!TextUtils.isEmpty(mSignUpModel?.first_name)) {
                et_firstName.setText(mSignUpModel?.first_name)
            }

            if (!TextUtils.isEmpty(mSignUpModel?.last_name)) {
                et_lastName.setText(mSignUpModel?.last_name)
            }

            if (!TextUtils.isEmpty(mSignUpModel?.email)) {
                et_email.setText(mSignUpModel?.email)
            }

            tv_passwordLabel.visibility = View.GONE
            et_password.visibility = View.GONE
            view_password.visibility = View.GONE
        } else {
            tv_passwordLabel.visibility = View.VISIBLE
            et_password.visibility = View.VISIBLE
            view_password.visibility = View.VISIBLE
        }
    }

    val termsAndCondition = object : ClickableSpan() {
        override fun onClick(textView: View) {
            val signUpIntent = Intent(this@SignUpActivity, TermsOfServiceActivity::class.java)
            startActivity(signUpIntent)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
    }

    val privacyPolicy = object : ClickableSpan() {
        override fun onClick(textView: View) {
            val signUpIntent = Intent(this@SignUpActivity, PrivacyPolicyActivity::class.java)
            startActivity(signUpIntent)
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }
    }

    private fun setupClickListener() {
        btn_signUp.setOnClickListener(this)
        iv_toolbarBack.setOnClickListener(this)
        lyt_dob.setOnClickListener(this)
        lyt_genderSpinner.setOnClickListener(this)
        checkbox_acceeptTermsAndCondition.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.lyt_genderSpinner -> {
                Utility.hideKeyboard(this, btn_signUp)
                spinner_gender.performClick()
            }
            R.id.lyt_dob -> {
                Utility.hideKeyboard(this, btn_signUp)
                showDateTimePickerDialog()
            }
            R.id.checkbox_acceeptTermsAndCondition -> {
                if (checkbox_acceeptTermsAndCondition.isChecked) {
                    checkbox_acceeptTermsAndCondition.buttonDrawable =
                        ContextCompat.getDrawable(this, R.mipmap.checked)
                } else {
                    checkbox_acceeptTermsAndCondition.buttonDrawable =
                        ContextCompat.getDrawable(this, R.mipmap.check)
                }
            }
            R.id.btn_signUp -> {
                Utility.hideKeyboard(this, btn_signUp)
                if (NetworkUtils.isNetworkAvailable(this@SignUpActivity)) {
                    if (isValidate()) {
                        if (checkbox_acceeptTermsAndCondition.isChecked) {
                            when (loginType) {
                                AppConstants.EMAIL_TYPE -> {
                                    createAccount()
                                }
                                AppConstants.FACEBOOK_TYPE -> {
                                    createNewUser(Prefs.getString(AppConstants.USER_ID))
                                }
                            }
                        }
                        else
                        {
                            ToastMsgUtils.showErrorMsg(
                                lyt_parent,
                                getString(R.string.error_msg_accept_term_and_condition)
                            )
                        }
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (TextUtils.isEmpty(et_firstName.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_first_name))
        } else if (TextUtils.isEmpty(et_lastName.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_last_name))
        } else if (TextUtils.isEmpty(et_dob.text.toString().trim())
        ) {
            isValid = false
            ToastMsgUtils.showErrorMsg(
                lyt_parent,
                getString(R.string.error_msg_empty_date_of_birth)
            )
        } else if (TextUtils.isEmpty(mSelectedGender)) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_gender))
        } else if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_email))
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString().trim()).matches()) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_invalid_email))
        } else if (loginType.equals(AppConstants.EMAIL_TYPE)) {
            if (TextUtils.isEmpty(et_password.text.toString().trim())) {
                isValid = false
                ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_empty_password))
            } else {
                isValid = true
            }
        } else {
            isValid = true
        }
        return isValid
    }

    private fun createAccount() {
        val emailText = et_email.text.toString().trim()
        val passwordText = et_password.text.toString().trim()

        lyt_bar.visibility = View.VISIBLE
        mAuth?.createUserWithEmailAndPassword(emailText, passwordText)!!
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    onAuthSuccess(task.getResult()?.getUser()!!)
                } else {
                    // If sign in fails, display a message to the user.
                    lyt_bar.visibility = View.GONE
                    ToastMsgUtils.showSuccessMsg(lyt_parent, "Authentication failed.")
                }
            }
    }

    private fun onAuthSuccess(user: FirebaseUser) {
        createNewUser(user.uid)
    }

    private fun createNewUser(userId: String) {
        lyt_bar.visibility = View.VISIBLE
        val db = FirebaseFirestore.getInstance()
        val updateData: MutableMap<String, Any> = HashMap()
        updateData["user_id"] = userId
        updateData["first_name"] = et_firstName.text.toString().trim()
        updateData["last_name"] = et_lastName.text.toString().trim()
        updateData["dob"] = mSelectedDateOfBirth!!
        updateData["email"] = et_email.text.toString().trim()
        updateData["gender"] = mSelectedGender
        updateData["lat"] = Prefs.getString(AppConstants.USER_LATITUDE)
        updateData["long"] = Prefs.getString(AppConstants.USER_LONGITUDE)
        updateData["date_created"] = Calendar.getInstance().timeInMillis
        // Add a new document with a generated ID
        val usersRef = db.collection("users")
        val id = userId
        usersRef.document(id).set(updateData)
            .addOnSuccessListener { documentReference ->
                //Log.d("Success", "DocumentSnapshot added with ID: ${documentReference.id}")
                Prefs.putString(AppConstants.USER_ID, userId)
                goToNextActivity()
                lyt_bar.visibility = View.GONE
            }
            .addOnFailureListener { e ->
                lyt_bar.visibility = View.GONE
                Log.w("Failure", "Error adding document", e)
            }
    }

    private fun goToNextActivity() {
        val signUpIntent = Intent(this@SignUpActivity, ProfilePickSelectionActivity::class.java)
        startActivity(signUpIntent)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    private fun showDateTimePickerDialog() {
//        Calendar instance = Calendar . getInstance ();
//        instance.add(Calendar.DATE, -1);
//        singleDateAndTimePicker.setDefaultDate(instance.getTime());

        val singleDateAndTimePicker = SingleDateAndTimePickerDialog.Builder(this)
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -1)
        singleDateAndTimePicker.defaultDate(calendar.time)
            .bottomSheet()
            .curved()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker?) {

                }
            })
            .title("")
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date?) {
                    mSelectedDateOfBirth = date?.time
                    et_dob.text = DateUtils.getFormatDOB(date!!, "dd-MM-yyyy")
                }
            })
            .display()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.spinner_gender -> {
                if (position != 0) {
                    mSelectedGender = parent.getSelectedItem() as String
                }
            }
        }
    }
}
