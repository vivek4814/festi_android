package com.muzified.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.muzified.R
import com.muzified.Utils.*
import com.muzified.adapter.GalleryImagesListAdapter
import com.muzified.dialog.DialogUtils
import com.muzified.model.SignUpModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_view_profile.*
import kotlinx.android.synthetic.main.activity_view_profile.lyt_bar
import kotlinx.android.synthetic.main.activity_view_profile.lyt_noResultFound
import kotlinx.android.synthetic.main.activity_view_profile.lyt_parent
import kotlinx.android.synthetic.main.activity_view_profile.lyt_resultFound
import kotlinx.android.synthetic.main.toolbar_profile_edit.*
import java.util.regex.Pattern

class ViewProfileActivity: AppCompatActivity(), View.OnClickListener {

    val db = Firebase.firestore
    private var mToolbarLayout: Toolbar? = null
    private var mSelectedProfileId: String? = null
    private var mUserGalleryImageList: ArrayList<String>? = null
    private var mGalleryImageListAdapter: GalleryImagesListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        setupUI()
        setupClickListener()
    }

    private fun setupUI()
    {
        if (intent.getStringExtra(AppConstants.USER_ID) != null)
        {
            mSelectedProfileId = intent.getStringExtra(AppConstants.USER_ID) as String
            getSelectedUserData()
        }
        tv_toolbarTitle.text = ""
        iv_toolbarEdit.visibility = View.GONE

        mUserGalleryImageList = ArrayList<String>()
        mGalleryImageListAdapter = GalleryImagesListAdapter(this, mUserGalleryImageList)
        val mLayoutManager = LinearLayoutManager(this@ViewProfileActivity, LinearLayoutManager.HORIZONTAL, false)
        rv_galleryImages.layoutManager = mLayoutManager
        rv_galleryImages.adapter = mGalleryImageListAdapter
        rv_galleryImages.hasFixedSize()
    }

    private fun updateUI(userModel: SignUpModel) {
        if(!TextUtils.isEmpty(userModel.profile))
        {
            Glide.with(this@ViewProfileActivity)
                .load(userModel.profile)
                .centerCrop()
                .placeholder(R.drawable.place_holder)
                .into(imageView)
        }
        if (!TextUtils.isEmpty(userModel.first_name))
        {
            tv_userName.text = userModel.first_name + " " + userModel.last_name
        }
        else
        {
            tv_userName.text = ""
        }

        tv_userAge.text = DateUtils.getCalculatedYearFromDate(userModel.dob!!)
        if (!TextUtils.isEmpty(userModel.bio))
        {
            tv_bio.text = userModel.bio
        }
        else
        {
            tv_bio.text = ""
        }

        val favouriteArtistList = AppConstants.getArtistList()
        val userFavouriteArtistIds = userModel.favourites_artist_ids
        var favouriteArtistName = ""
        if (!userFavouriteArtistIds.isNullOrEmpty())
        {
            if (!favouriteArtistList.isNullOrEmpty())
            {
                for (j in 0 until userFavouriteArtistIds.size)
                {
                    val favouriteArtistId = userFavouriteArtistIds.get(j)
                    for (i in 0 until favouriteArtistList.size)
                    {
                        val artistModel = favouriteArtistList.get(i)
                        if (favouriteArtistId.equals(artistModel.id)){
                            if (!TextUtils.isEmpty(favouriteArtistName))
                            {
                                favouriteArtistName = favouriteArtistName + ", " + artistModel.FavoriteArtistName
                            }
                            else
                            {
                                favouriteArtistName = artistModel.FavoriteArtistName
                            }
                            break
                        }
                    }
                }
            }
        }

        tv_favoriteArtist.text = favouriteArtistName
        if (!TextUtils.isEmpty(userModel.lat) && !TextUtils.isEmpty(userModel.long))
        {
            val userLat = Prefs.getString(AppConstants.USER_LATITUDE).toDouble()
            val userLong = Prefs.getString(AppConstants.USER_LONGITUDE).toDouble()
            val distance = Utility.calculateDistanceBetweenTwoLatLong(userLat, userLong, userModel.lat.toDouble(), userModel.long.toDouble())
            val calculatedDistance = distance.toInt()
            tv_distance.text = calculatedDistance.toString() +  " mile(s)"
        }
        else
        {
            tv_distance.text = ""
        }

        val favouriteMusicList = AppConstants.getFavouriteMusicList()
        val userFavouriteMusicIds = userModel.favourites_music_ids
        var favouriteMusicName = ""

        if (!userFavouriteMusicIds.isNullOrEmpty())
        {
            for (j in 0 until userFavouriteMusicIds.size)
            {
                val favouriteMusicId = userFavouriteMusicIds.get(j)
                for (i in 0 until favouriteMusicList.size)
                {
                    val musicModel = favouriteMusicList.get(i)
                    if (favouriteMusicId.equals(musicModel.id)){
                        if (!TextUtils.isEmpty(favouriteMusicName))
                        {
                            favouriteMusicName = favouriteMusicName + ", " + musicModel.FavoriteMusicName
                        }
                        else
                        {
                            favouriteMusicName = musicModel.FavoriteMusicName
                        }
                        break
                    }
                }
            }
        }

        tv_favoriteGenres.text = favouriteMusicName

        if (!TextUtils.isEmpty(userModel.gallery_image0))
        {
            mUserGalleryImageList?.add(userModel.gallery_image0)
        }
        if (!TextUtils.isEmpty(userModel.gallery_image1))
        {
            mUserGalleryImageList?.add(userModel.gallery_image1)
        }
        if (!TextUtils.isEmpty(userModel.gallery_image2))
        {
            mUserGalleryImageList?.add(userModel.gallery_image2)
        }
        if (!TextUtils.isEmpty(userModel.gallery_image3))
        {
            mUserGalleryImageList?.add(userModel.gallery_image3)
        }
        if (!TextUtils.isEmpty(userModel.gallery_image4))
        {
            mUserGalleryImageList?.add(userModel.gallery_image4)
        }
        mGalleryImageListAdapter?.notifyDataSetChanged()
    }

    private fun setupClickListener()
    {
        iv_toolbarBack.setOnClickListener(this)
        iv_option.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_toolbarBack ->
            {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
            R.id.iv_option ->
            {
                showBottomSheetDialog()
            }
        }
    }

    private fun getSelectedUserData()
    {
        lyt_bar.visibility = View.VISIBLE
        db.collection("users")
            .whereEqualTo("user_id", mSelectedProfileId)
            .get()
            .addOnSuccessListener { result ->
                if (!result.isEmpty) {
                    val userModel = SignUpModel()
                    for (document in result) {
                        val userData = document.data
                        if (userData.contains("user_id") && !TextUtils.isEmpty(userData.get("user_id") as String)) {
                            userModel.user_id = userData.get("user_id") as String
                        }
                        if (userData.contains("isProfileCompleted") && userData.get("isProfileCompleted") != null) {
                            userModel.isProfileCompleted =
                                userData.get("isProfileCompleted") as Boolean
                        }
                        if (userData.contains("first_name") && !TextUtils.isEmpty(userData.get("first_name") as String)) {
                            userModel.first_name = userData.get("first_name") as String
                        }
                        if (userData.contains("last_name") && !TextUtils.isEmpty(userData.get("last_name") as String)) {
                            userModel.last_name = userData.get("last_name") as String
                        }
                        if (userData.contains("dob") && !TextUtils.isEmpty(userData.get("dob").toString()))
                        {
                            val dateStr = userData.get("dob").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.dob = userData.get("dob") as Long
                            }

                        }
                        if (userData.contains("email") && !TextUtils.isEmpty(userData.get("email") as String)) {
                            userModel.email = userData.get("email") as String
                        }
                        if (userData.contains("gender") && !TextUtils.isEmpty(userData.get("gender") as String)) {
                            userModel.gender = userData.get("gender") as String
                        }
                        if (userData.contains("profile") && !TextUtils.isEmpty(userData.get("profile") as String)) {
                            userModel.profile = userData.get("profile") as String
                        }
                        if (userData.contains("bio") && !TextUtils.isEmpty(userData.get("bio") as String)) {
                            userModel.bio = userData.get("bio") as String
                        }
                        if (userData.contains("lat") && !TextUtils.isEmpty(userData.get("lat") as String)) {
                            userModel.lat = userData.get("lat") as String
                        }
                        if (userData.contains("long") && !TextUtils.isEmpty(userData.get("long") as String)) {
                            userModel.long = userData.get("long") as String
                        }
                        if (userData.contains("gender_preference") && !TextUtils.isEmpty(
                                userData.get(
                                    "gender_preference"
                                ) as String
                            )
                        ) {
                            userModel.gender_preference =
                                userData.get("gender_preference") as String
                        }
                        if (userData.contains("gallery_image0") && !TextUtils.isEmpty(userData.get("gallery_image0") as String)) {
                            userModel.gallery_image0 = userData.get("gallery_image0") as String
                        }
                        if (userData.contains("gallery_image1") && !TextUtils.isEmpty(userData.get("gallery_image1") as String)) {
                            userModel.gallery_image1 = userData.get("gallery_image1") as String
                        }
                        if (userData.contains("gallery_image2") && !TextUtils.isEmpty(userData.get("gallery_image2") as String)) {
                            userModel.gallery_image2 = userData.get("gallery_image2") as String
                        }
                        if (userData.contains("gallery_image3") && !TextUtils.isEmpty(userData.get("gallery_image3") as String)) {
                            userModel.gallery_image3 = userData.get("gallery_image3") as String
                        }
                        if (userData.contains("gallery_image4") && !TextUtils.isEmpty(userData.get("gallery_image4") as String)) {
                            userModel.gallery_image4 = userData.get("gallery_image4") as String
                        }
                        if (userData.contains("favourites_artist_ids") && userData.get("favourites_artist_ids") != null) {
                            userModel.favourites_artist_ids =
                                userData.get("favourites_artist_ids") as ArrayList<String>
                        }
                        if (userData.contains("favourites_music_ids") && userData.get("favourites_music_ids") != null) {
                            userModel.favourites_music_ids =
                                userData.get("favourites_music_ids") as ArrayList<String>
                        }
//                        if (userData.contains("date_created") && userData.get("date_created") != null) {
//                            userModel.date_created = userData.get("date_created") as Long
//                        }
                        if (userData.contains("date_created") && !TextUtils.isEmpty(userData.get("date_created").toString()))
                        {
                            val dateStr = userData.get("date_created").toString()
                            val p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
                            val m = p.matcher(dateStr)
                            val b: Boolean = m.find()

                            if (b)
                            {
                                //10/27/2021
//                                val convertedDateTime = DateUtils.convertStringToDate(userData.get("dob").toString())
//                                userModel.dob = convertedDateTime
                            }
                            else
                            {
                                userModel.date_created = userData.get("date_created") as Long
                            }

                        }
                    }
                    lyt_bar.visibility = View.GONE
                    lyt_noResultFound.visibility = View.GONE
                    lyt_resultFound.visibility = View.VISIBLE
                    updateUI(userModel)
                } else {
                    lyt_noResultFound.visibility = View.VISIBLE
                    lyt_resultFound.visibility = View.GONE
                    lyt_bar.visibility = View.GONE
                }
            }
            .addOnFailureListener { exception ->
                lyt_bar.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(lyt_parent, "" + exception.message)
            }
    }

    private fun showBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(this@ViewProfileActivity, R.style.CustomBottomSheetDialogTheme)
        val sheetView: View = layoutInflater.inflate(R.layout.dialog_block_report_cancel, null)
        bottomSheetDialog.setContentView(sheetView)
        bottomSheetDialog.show()
        val blockBtn = bottomSheetDialog.findViewById<TextView>(R.id.tv_block)
        val reportBtn = bottomSheetDialog.findViewById<TextView>(R.id.tv_report)
        val cancelBtn = bottomSheetDialog.findViewById<CardView>(R.id.lyt_dismissDialog)
        blockBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                bottomSheetDialog.dismiss()
                DialogUtils.openAlertDialog(this@ViewProfileActivity, "User has been blocked from viewing your account")
            }
        })
        reportBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                bottomSheetDialog.dismiss()
                DialogUtils.openAlertDialog(this@ViewProfileActivity, "Thank you for your submission. Your request will be reviewed shortly.")
            }
        })
        cancelBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                bottomSheetDialog.dismiss()
            }
        })
        bottomSheetDialog.show()
    }
}