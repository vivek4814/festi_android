package com.muzified.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.muzified.R
import kotlinx.android.synthetic.main.activity_account_type_seletion.*

class AccountSelectionActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_type_seletion)

        setupClickListener()
    }

    private fun setupClickListener()
    {
        iv_newUserType.setOnClickListener(this)
        iv_existingUserType.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.iv_newUserType ->
            {
                val signUpIntent = Intent(this@AccountSelectionActivity, AccountCreationTypeActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
            R.id.iv_existingUserType ->
            {
                val signUpIntent = Intent(this@AccountSelectionActivity, LoginActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }
    }
}
