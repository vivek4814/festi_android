package com.muzified.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.NetworkUtils
import com.muzified.Utils.Prefs
import com.muzified.Utils.ToastMsgUtils
import com.muzified.adapter.ChatRequestListAdapter
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.ChatRequestModel
import com.muzified.model.SignUpModel
import com.muzified.model.conversation.ConversationResultModel
import kotlinx.android.synthetic.main.activity_chat_request.*
import kotlinx.android.synthetic.main.activity_chat_request.lyt_bar
import kotlinx.android.synthetic.main.activity_chat_request.lyt_parent
import kotlinx.android.synthetic.main.activity_chat_request.lyt_resultFound
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar_main.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChatRequestActivity : AppCompatActivity(), View.OnClickListener,
    ItemClickListenerWithViewType {

    val db = Firebase.firestore
    private var mToolbarLayout: Toolbar? = null
    private var mChatRequestList: ArrayList<ChatRequestModel>? = null
    private var mChatRequestListAdapter: ChatRequestListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_request)
        mToolbarLayout = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbarLayout)
        initialiseUI()
        setupClickListener()
        getChatRequestList()
    }

    private fun initialiseUI() {
        tv_toolbarTitle.text = resources.getString(R.string.chat_request)
        mChatRequestList = ArrayList<ChatRequestModel>()
        mChatRequestListAdapter = ChatRequestListAdapter(this, mChatRequestList!!, this)
        val mLayoutManager = LinearLayoutManager(this@ChatRequestActivity)
        rv_chatRequest.layoutManager = mLayoutManager
        rv_chatRequest.adapter = mChatRequestListAdapter
        rv_chatRequest.hasFixedSize()

        et_searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                if (editable?.length!! > 0) {
                    iv_clear.visibility = View.VISIBLE
                } else {
                    iv_clear.visibility = View.GONE
                }
                mChatRequestListAdapter?.filter?.filter(editable)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun setupClickListener() {
        iv_toolbarBack.setOnClickListener(this)
        iv_clear?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
            }
            R.id.iv_clear -> {
                et_searchView.setText("")
                et_searchView.hideKeyboard()
                iv_clear.visibility = View.GONE
                mChatRequestListAdapter?.filter?.filter("")
            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun getChatRequestList() {
        if (NetworkUtils.isNetworkAvailable(this@ChatRequestActivity)) {
            lyt_bar.visibility = View.VISIBLE
            db.collection("request")
                .whereEqualTo("to_id", Prefs.getString(AppConstants.USER_ID))
                .get()
                .addOnSuccessListener { result ->
                    if (!result.isEmpty) {
                        val requestList = ArrayList<ChatRequestModel>()
                        for (document in result) {
                            val requestModel = ChatRequestModel()
                            val requestData = document.data
                            if (requestData.contains("request_id") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "request_id"
                                    ) as String
                                )
                            ) {
                                requestModel.request_id = requestData.get("request_id") as String
                            }
                            if (requestData.contains("to_id") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "to_id"
                                    ) as String
                                )
                            ) {
                                requestModel.to_id = requestData.get("to_id") as String
                            }
                            if (requestData.contains("from_id") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "from_id"
                                    ) as String
                                )
                            ) {
                                requestModel.from_id = requestData.get("from_id") as String
                            }
                            if (requestData.contains("first_name") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "first_name"
                                    ) as String
                                )
                            ) {
                                requestModel.first_name = requestData.get("first_name") as String
                            }
                            if (requestData.contains("last_name") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "last_name"
                                    ) as String
                                )
                            ) {
                                requestModel.last_name = requestData.get("last_name") as String
                            }
                            if (requestData.contains("from_email") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "from_email"
                                    ) as String
                                )
                            ) {
                                requestModel.from_email = requestData.get("from_email") as String
                            }
                            if (requestData.contains("from_image") && !TextUtils.isEmpty(
                                    requestData.get(
                                        "from_image"
                                    ) as String
                                )
                            ) {
                                requestModel.from_image = requestData.get("from_image") as String
                            }
                            requestList.add(requestModel)
                        }
                        lyt_bar.visibility = View.GONE
                        if (!requestList.isNullOrEmpty()) {
                            tv_noResultFound.visibility = View.GONE
                            lyt_resultFound.visibility = View.VISIBLE
                            if (!mChatRequestList.isNullOrEmpty()) {
                                mChatRequestList?.clear()
                            }
                            mChatRequestList?.addAll(requestList)
                            mChatRequestListAdapter?.notifyDataSetChanged()
                        } else {
                            tv_noResultFound.visibility = View.VISIBLE
                            lyt_resultFound.visibility = View.GONE
                        }
                    } else {
                        lyt_bar.visibility = View.GONE
                        tv_noResultFound.visibility = View.VISIBLE
                        lyt_resultFound.visibility = View.GONE
                    }
                }
                .addOnFailureListener { exception ->
                    lyt_bar.visibility = View.GONE
                }
        } else {
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_no_network))
        }
    }

    override fun onItemClick(position: Int, view: View) {
        val requestUserData = mChatRequestList?.get(position)
        when (view.id) {
            R.id.lyt_parent -> {
                val profileIntent =
                    Intent(this@ChatRequestActivity, ViewProfileActivity::class.java)
                profileIntent.putExtra(AppConstants.USER_ID, requestUserData?.from_id)
                startActivity(profileIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
            }
            R.id.iv_acceptRequest -> {
                createChatRequest(position, requestUserData)
            }
            R.id.iv_rejectRequest -> {
                manageRejectRequest(position, requestUserData?.request_id)
            }
        }
    }

    private fun createChatRequest(position: Int, requestUserData: ChatRequestModel?) {
        lyt_bar.visibility = View.VISIBLE
        val useId = Prefs.getString(AppConstants.USER_ID)

        db.collection("Conversations")
            .whereArrayContains("userIDs", useId)
            .get()
            .addOnSuccessListener { result ->
                if (result.isEmpty) {
                    sendNewConversationRequest(position, requestUserData)
                } else {
                    val previousConversationList = ArrayList<ConversationResultModel>()
                    for (document in result) {
                        val conversationData = document.data
                        val conversationResult = ConversationResultModel()
                        if (conversationData.contains("id") && !TextUtils.isEmpty(conversationData.get("id") as String)) {
                            conversationResult.id = conversationData.get("id") as String
                        }
                        if (conversationData.contains("isRead") && !(conversationData.get("isRead") as HashMap<*, *>).isNullOrEmpty()) {
                            conversationResult.isRead = conversationData.get("isRead") as HashMap<*, *>
                        }
                        if (conversationData.contains("userIDs")) {
                            conversationResult.userIDs = conversationData.get("userIDs") as ArrayList<String>
                        }

                        if (conversationData.contains("lastMessage")) {
                            conversationResult.lastMessage = conversationData.get("lastMessage") as String
                        }

                        previousConversationList.add(conversationResult)
                    }
                    if (!previousConversationList.isNullOrEmpty())
                    {
                        var isPreviousConversationFound = false
                        var previousConversationId = ""
                        for(i in 0 until previousConversationList.size)
                        {
                            val item = previousConversationList.get(i)
                            val userIds = item.userIDs
                            if (userIds?.contains(requestUserData?.from_id)!!)
                            {
                                isPreviousConversationFound = true
                                if (!TextUtils.isEmpty(item.lastMessage))
                                {
                                    previousConversationId = item.id
                                    break
                                }
                                else
                                {
                                    previousConversationId = item.id
                                }
                            }
                        }

                        if (isPreviousConversationFound)
                        {
                            val conversationId = previousConversationId
                            manageAcceptRequest(position, requestUserData!!, conversationId)
                            ToastMsgUtils.showErrorMsg(lyt_parent, "Previous conversation found")
                        }
                        else
                        {
                            sendNewConversationRequest(position, requestUserData)
                            //ToastMsgUtils.showErrorMsg(lyt_parent, "Previous conversation not found")
                        }
                    }
                }
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
            }
    }

    private fun sendNewConversationRequest(position: Int, requestUserData: ChatRequestModel?)
    {
        val useId = Prefs.getString(AppConstants.USER_ID)
        val conversationId = UUID.randomUUID().toString().toUpperCase()
        val timestamp = Calendar.getInstance().timeInMillis

        val isReadMap = HashMap<String, Boolean>()
        isReadMap[requestUserData?.from_id!!] = true
        isReadMap[useId] = true

        val userIdsArrayList = ArrayList<String>()
        userIdsArrayList.add(requestUserData.from_id)
        userIdsArrayList.add(useId)

        val createConversationMap = HashMap<String, Any>()
        createConversationMap["id"] = conversationId
        createConversationMap["isRead"] = isReadMap
        createConversationMap["timestamp"] = timestamp
        createConversationMap["lastMessage"] = ""
        createConversationMap["userIDs"] = userIdsArrayList

        try {
            db.collection("Conversations").document(conversationId).set(createConversationMap)
                .addOnSuccessListener { documentReference ->
                    manageAcceptRequest(position, requestUserData, conversationId)
                }
                .addOnFailureListener { e ->
                    lyt_bar.visibility = View.GONE
                    Log.w("Failure", "Error adding document", e)
                }
        } catch (e: Exception) {
            lyt_bar.visibility = View.GONE
            Log.w("Exception", "Exception adding document", e)
        }
    }

    private fun manageAcceptRequest(position: Int, requestUserData: ChatRequestModel, conversationId: String) {
        db.collection("request").document(requestUserData.request_id)
            .delete()
            .addOnSuccessListener {
                lyt_bar.visibility = View.GONE

                val conversationModel = ConversationResultModel()
                conversationModel.id = conversationId
                val userModel = SignUpModel()
                userModel.first_name = requestUserData.first_name
                userModel.last_name = requestUserData.last_name
                userModel.profile = requestUserData.from_image
                conversationModel.userProfileModel = userModel

                moveToChatMessageActivity(conversationModel)
                mChatRequestList?.removeAt(position)
                mChatRequestListAdapter?.notifyItemRemoved(position)
                if (mChatRequestList.isNullOrEmpty())
                {
                    tv_noResultFound.visibility = View.VISIBLE
                    lyt_resultFound.visibility = View.GONE
                }
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
            }
    }

    private fun manageRejectRequest(position: Int, requestId: String?) {
        db.collection("request").document(requestId!!)
            .delete()
            .addOnSuccessListener {
                mChatRequestList?.removeAt(position)
                mChatRequestListAdapter?.notifyItemChanged(position)
                if (mChatRequestList.isNullOrEmpty()) {
                    tv_noResultFound.visibility = View.VISIBLE
                    lyt_resultFound.visibility = View.GONE
                }
            }
            .addOnFailureListener {
                lyt_bar.visibility = View.GONE
            }
    }

    private fun moveToChatMessageActivity(conversationModel: ConversationResultModel) {
        val chatMessageIntent = Intent(this@ChatRequestActivity, ChatMessageActivity::class.java)
        chatMessageIntent.putExtra(AppConstants.SELECTED_CONVERSATION_ID, conversationModel)
        startActivity(chatMessageIntent)
    }
}