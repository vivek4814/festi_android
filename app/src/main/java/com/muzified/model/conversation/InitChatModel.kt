package com.muzified.model.conversation

data class InitChatModel (
    var project_id: String = "",
    var participant_default_profile_id: Array<String>? = null
)
