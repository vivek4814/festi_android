package com.muzified.model.conversation

import com.muzified.model.SignUpModel
import java.io.Serializable

data class ConversationResultModel (
    var id: String = "",
    var isRead: HashMap<*, *>? = null,
    var lastMessage: String = "",
    var timestamp: Long = 0,
    var userIDs: ArrayList<String>? = null,
    var Messages: ArrayList<MessageModel>? = null,
    var userProfileModel: SignUpModel? = null,
    var resultType: Boolean = false
): Serializable