package com.muzified.model.conversation

data class MessageModel (
    var id: String = "",
    var contentType: Long = -1,
    var ownerID: String = "",
    var timestamp: Long = 0,
    var videoLink: String = "",
    var message: String = "",
    var profilePicLink: String ="",
    var userProfileImage: String =""
)