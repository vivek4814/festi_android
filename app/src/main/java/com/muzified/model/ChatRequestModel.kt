package com.muzified.model

import java.io.Serializable

data class ChatRequestModel (
    var request_id: String = "",
    var to_id: String = "",
    var from_id: String = "",
    var first_name: String = "",
    var last_name: String = "",
    var from_email: String = "",
    var from_image: String = "",
    var resultType: Boolean = false
): Serializable