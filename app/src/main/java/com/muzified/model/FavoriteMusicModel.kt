package com.muzified.model

data class FavoriteMusicModel (
    var id: String = "",
    var FavoriteMusicName: String = "",
    var isSelected: Boolean = false,
    var resultType: Boolean = false
)