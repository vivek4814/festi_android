package com.muzified.model

data class FavoriteArtistModel (
    var id: String = "",
    var FavoriteArtistName: String = "",
    var Image: Int = 0,
    var isSelected: Boolean = false,
    var resultType: Boolean = false
)