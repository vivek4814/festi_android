package com.muzified.model

import java.io.Serializable

data class SignUpModel (
    var user_id: String = "",
    var first_name: String = "",
    var last_name: String = "",
    var dob: Long = 0,
    var email: String = "",
    var gender: String = "",
    var profile: String = "",
    var bio: String = "",
    var lat: String = "",
    var long: String = "",
    var isProfileCompleted: Boolean = false,
    var gender_preference: String = "",
    var gallery_image0: String = "",
    var gallery_image1: String = "",
    var gallery_image2: String = "",
    var gallery_image3: String = "",
    var gallery_image4: String = "",
    var favourites_artist_ids: ArrayList<String>? = null,
    var favourites_music_ids: ArrayList<String>? = null,
    var date_created: Long = 0
): Serializable