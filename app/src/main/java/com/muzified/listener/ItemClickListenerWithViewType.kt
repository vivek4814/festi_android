package com.muzified.listener

import android.view.View
import java.text.FieldPosition

interface ItemClickListenerWithViewType {
    fun onItemClick(position: Int, view: View)
}