package com.muzified.listener

import java.text.FieldPosition

interface ItemClickListener {
    fun onItemClick(position: Int)
}