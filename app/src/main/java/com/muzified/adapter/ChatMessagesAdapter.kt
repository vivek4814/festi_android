package com.muzified.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.DateUtils
import com.muzified.Utils.Prefs
import com.muzified.listener.ItemClickListener
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.conversation.ConversationResultModel
import com.muzified.model.conversation.MessageModel

class ChatMessagesAdapter (context: Context, chatUserList: ArrayList<MessageModel>,
                           listener: ItemClickListenerWithViewType): RecyclerView.Adapter<ChatMessagesAdapter.MyViewHolder>() {

    private var mContext: Context? = null
    private var mChatList: ArrayList<MessageModel>? = null
    private var itemClickListener: ItemClickListenerWithViewType? = null
    private val MSG_TYPE_LEFT = 0
    private val MSG_TYPE_RIGHT = 1

    init {
        this.mContext = context
        this.mChatList = chatUserList
        this.itemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return if (viewType == MSG_TYPE_RIGHT) {
            val view: View = LayoutInflater.from(mContext).inflate(R.layout.chat_item_right, parent, false)
            MyViewHolder(view)
        } else {
            val view: View = LayoutInflater.from(mContext).inflate(R.layout.chat_item_left, parent, false)
            MyViewHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = mChatList?.get(position)
        return if (item?.ownerID.equals(Prefs.getString(AppConstants.USER_ID))) {
            MSG_TYPE_RIGHT
        } else {
            MSG_TYPE_LEFT
        }
    }

    override fun getItemCount(): Int {
        return mChatList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val chatItem = mChatList?.get(position)

        if (!TextUtils.isEmpty(chatItem?.userProfileImage))
        {
            Glide.with(mContext!!)
                .load(chatItem?.userProfileImage)
                .centerCrop()
                .placeholder(R.drawable.place_holder)
                .into(holder.mProfileImage!!)
        }

        val messageType = chatItem?.contentType?.toInt()
        when(messageType)
        {
            AppConstants.MSG_TYPE_TEXT ->{
                holder.mReceiverMsg?.visibility = View.VISIBLE
                holder.mImageMsgType?.visibility = View.GONE

                holder.mReceiverMsg?.text = chatItem.message
                holder.mMsgDateTime?.text = DateUtils.getDateAndTime(chatItem.timestamp, "HH:mm a")
            }
            AppConstants.MSG_TYPE_IMAGE ->{
                holder.mReceiverMsg?.visibility = View.GONE
                holder.mImageMsgType?.visibility = View.VISIBLE

                Glide.with(mContext!!)
                    .load(chatItem.profilePicLink)
                    .centerCrop()
                    .placeholder(R.mipmap.gallery_placeholder)
                    .into(holder.mMsgImage!!)

                holder.mPlayVideoIcon?.visibility = View.GONE
                holder.mMsgDateTime?.text = DateUtils.getDateAndTime(chatItem.timestamp, "HH:mm a")
            }
            AppConstants.MSG_TYPE_VIDEO ->{
                holder.mReceiverMsg?.visibility = View.GONE
                holder.mImageMsgType?.visibility = View.VISIBLE

                Glide.with(mContext!!)
                    .load(chatItem.videoLink)
                    .centerCrop()
                    .placeholder(R.mipmap.gallery_placeholder)
                    .into(holder.mMsgImage!!)

                holder.mPlayVideoIcon?.visibility = View.VISIBLE
                holder.mMsgDateTime?.text = DateUtils.getDateAndTime(chatItem.timestamp, "HH:mm a")
            }
        }

        holder.mImageMsgType?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                itemClickListener?.onItemClick(position, v!!)
            }
        })

        holder.mPlayVideoIcon?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                itemClickListener?.onItemClick(position, holder.mPlayVideoIcon!!)
            }
        })
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mProfileImage: ImageView? = null
        var mReceiverMsg: TextView? = null
        var mMsgDateTime: TextView? = null
        var mMsgImage: ImageView? = null

        var mImageMsgType: RelativeLayout? = null
        var mPlayVideoIcon: ImageView? = null

        init {
            mProfileImage = itemView.findViewById(R.id.iv_senderUserImage)
            mReceiverMsg = itemView.findViewById(R.id.tv_receiverMsg)
            mMsgDateTime = itemView.findViewById(R.id.tv_msgDateTime)
            mMsgImage = itemView.findViewById(R.id.iv_msgImage)

            mImageMsgType = itemView.findViewById(R.id.lyt_imageMsgType)
            mPlayVideoIcon = itemView.findViewById(R.id.iv_playVideo)
        }
    }
}