package com.muzified.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.listener.ItemClickListener
import com.muzified.model.ChatRequestModel
import com.muzified.model.FavoriteArtistModel
import java.util.*
import kotlin.collections.ArrayList

class FavoriteArtistAdapter(
    context: Context,
    favoriteArtistList: ArrayList<FavoriteArtistModel>?,
    itemClickListener: ItemClickListener
) : RecyclerView.Adapter<FavoriteArtistAdapter.MyViewHolder>(),
    Filterable {

    private var mContext: Context? = null
    private var mFavoriteArtistList: ArrayList<FavoriteArtistModel>? = null
    private var mFilterFavoriteArtistList: ArrayList<FavoriteArtistModel>? = null
    private var mItemClickListener: ItemClickListener? = null
    private var mFilterResult: Filter? = null

    init {
        this.mContext = context
        this.mFavoriteArtistList = favoriteArtistList
        this.mFilterFavoriteArtistList = favoriteArtistList
        this.mItemClickListener = itemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_favorite_artist_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mFilterFavoriteArtistList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val artistData = mFilterFavoriteArtistList?.get(position)

        if (!artistData?.resultType!!) {
            holder.tv_noResultFound?.visibility = View.GONE
            holder.cardViewResultFound?.visibility = View.VISIBLE

            holder.mFavoriteArtistName?.text = artistData?.FavoriteArtistName
            Glide.with(mContext!!)
                .load(artistData?.Image)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.mFavoriteArtistImage!!)

            if (artistData?.isSelected!!) {
                holder.mFavorite?.setImageResource(R.mipmap.heart_selected)
            } else {
                holder.mFavorite?.setImageResource(R.mipmap.heart)
            }
        } else {
            holder.tv_noResultFound?.visibility = View.VISIBLE
            holder.cardViewResultFound?.visibility = View.GONE
        }

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (!artistData?.resultType!!) {
                    if (artistData.isSelected) {
                        holder.mFavorite?.setImageResource(R.mipmap.heart)
                        artistData.isSelected = false
                    } else {
                        holder.mFavorite?.setImageResource(R.mipmap.heart_selected)
                        artistData.isSelected = true
                    }
                    notifyDataSetChanged()
                }
            }
        })
    }

    override fun getFilter(): Filter {
        if (mFilterResult == null) {
            mFilterResult = RecordFilter()
        }
        return mFilterResult!!
    }

    inner class RecordFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charString = constraint.toString()
            if (charString.isEmpty()) {
                mFilterFavoriteArtistList = mFavoriteArtistList
            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                val filteredList = ArrayList<FavoriteArtistModel>()

                for (row in mFavoriteArtistList!!) {
                    if (row.FavoriteArtistName.toLowerCase(Locale.ROOT).contains(
                            charString.toLowerCase(
                                Locale.ROOT
                            )
                        ) ||
                        row.FavoriteArtistName.contains(charString)
                    ) {
                        filteredList.add(row)
                    }
                }
                if (filteredList.size == 0) {
                    val noDataBean = FavoriteArtistModel()
                    noDataBean.FavoriteArtistName = "No result found."
                    noDataBean.resultType = true
                    filteredList.add(noDataBean)
                }
                mFilterFavoriteArtistList = filteredList
            }
            val filterResults = Filter.FilterResults()
            filterResults.values = mFilterFavoriteArtistList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mFilterFavoriteArtistList = results?.values as ArrayList<FavoriteArtistModel>
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mFavoriteArtistImage: ImageView? = null
        var mFavoriteArtistName: TextView? = null
        var mFavorite: ImageView? = null
        var tv_noResultFound: TextView? = null
        var cardViewResultFound: CardView? = null

        init {
            mFavoriteArtistImage = itemView.findViewById(R.id.profile_image)
            mFavoriteArtistName = itemView.findViewById(R.id.tv_artistName)
            mFavorite = itemView.findViewById(R.id.iv_favorite)
            tv_noResultFound = itemView.findViewById(R.id.tv_noResultFound)
            cardViewResultFound = itemView.findViewById(R.id.lyt_resultFound)
        }
    }
}