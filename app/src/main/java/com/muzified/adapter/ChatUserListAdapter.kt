package com.muzified.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.Utils.DateUtils
import com.muzified.listener.ItemClickListener
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.ChatRequestModel
import com.muzified.model.FavoriteArtistModel
import com.muzified.model.conversation.ConversationResultModel
import java.util.*
import kotlin.collections.ArrayList

class ChatUserListAdapter(context: Context, requestList: ArrayList<ConversationResultModel>, itemListener: ItemClickListener) : RecyclerView.Adapter<ChatUserListAdapter.MyViewHolder>(),
    Filterable {

    private var mContext: Context? = null
    private var mChatRequestList: ArrayList<ConversationResultModel>? = null
    private var mFilterChatRequestList: ArrayList<ConversationResultModel>? = null
    private var mClickListener: ItemClickListener? = null
    private var mFilterResult: Filter? = null

    init {
        this.mContext = context
        this.mChatRequestList = requestList
        this.mFilterChatRequestList = requestList
        this.mClickListener = itemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_chat_user_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mFilterChatRequestList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = mFilterChatRequestList?.get(position)
        if (!item?.resultType!!)
        {
            holder.tv_noResultFound?.visibility = View.GONE
            holder.cardViewResultFound?.visibility = View.VISIBLE

            val chatUserData = item.userProfileModel

            if (!TextUtils.isEmpty(chatUserData?.profile))
            {
                Glide.with(mContext!!)
                    .load(chatUserData?.profile)
                    .centerCrop()
                    .placeholder(R.drawable.place_holder)
                    .into(holder.mUserProfileImage!!)
            }

            if (!TextUtils.isEmpty(chatUserData?.first_name))
            {
                holder.mUserName?.text = chatUserData?.first_name + " " + chatUserData?.last_name
            }

            if (!TextUtils.isEmpty(item.lastMessage))
            {
                holder.mLastMsg?.text = item.lastMessage
            }
            holder.mLastMsgDateTime?.text = DateUtils.getDateAndTime(item.timestamp, "MMM,dd yyyy")
        }
        else
        {
            holder.tv_noResultFound?.visibility = View.VISIBLE
            holder.cardViewResultFound?.visibility = View.GONE
        }


        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                if (!item.resultType)
                {
                    mClickListener?.onItemClick(position)
                }
            }
        })
    }

    override fun getFilter(): Filter {
        if (mFilterResult == null) {
            mFilterResult = RecordFilter()
        }
        return mFilterResult!!
    }

    inner class RecordFilter: Filter()
    {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charString = constraint.toString()
            if (charString.isEmpty()) {
                mFilterChatRequestList = mChatRequestList
            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                val filteredList = ArrayList<ConversationResultModel>()

                for (row in mChatRequestList!!) {
                    if (row.userProfileModel?.first_name?.toLowerCase(Locale.ROOT)!!.contains(charString.toLowerCase(
                            Locale.ROOT)) ||
                        row.userProfileModel?.first_name!!.contains(charString))
                    {
                        filteredList.add(row)
                    }
                }
                if (filteredList.size == 0) {
                    val noDataBean = ConversationResultModel()
                    noDataBean.lastMessage = "No result found."
                    noDataBean.resultType = true
                    filteredList.add(noDataBean)
                }
                mFilterChatRequestList = filteredList
            }
            val filterResults = Filter.FilterResults()
            filterResults.values = mFilterChatRequestList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mFilterChatRequestList = results?.values as ArrayList<ConversationResultModel>
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mUserProfileImage: ImageView? = null
        var mUserName: TextView? = null
        var mLastMsgDateTime: TextView? = null
        var mLastMsg: TextView? = null
        var tv_noResultFound: TextView? = null
        var cardViewResultFound: CardView? = null

        init {
            mUserProfileImage = itemView.findViewById(R.id.iv_profileImage)
            mUserName = itemView.findViewById(R.id.tv_userName)
            mLastMsgDateTime = itemView.findViewById(R.id.tv_dateTime)
            mLastMsg = itemView.findViewById(R.id.tv_message)
            tv_noResultFound = itemView.findViewById(R.id.tv_noResultFound)
            cardViewResultFound = itemView.findViewById(R.id.lyt_resultFound)
        }
    }
}