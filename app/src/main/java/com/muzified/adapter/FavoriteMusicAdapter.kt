package com.muzified.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.muzified.R
import com.muzified.model.FavoriteArtistModel
import com.muzified.model.FavoriteMusicModel
import java.util.*
import kotlin.collections.ArrayList

class FavoriteMusicAdapter(
    context: Context,
    favoriteMusicList: ArrayList<FavoriteMusicModel>?
) : RecyclerView.Adapter<FavoriteMusicAdapter.MyViewHolder>(), Filterable {

    private var mContext: Context? = null
    private var mFavoriteMusicList: ArrayList<FavoriteMusicModel>? = null
    private var mFilterFavoriteMusicList: ArrayList<FavoriteMusicModel>? = null
    private var mFilterResult: Filter? = null

    init {
        this.mContext = context
        this.mFavoriteMusicList = favoriteMusicList
        this.mFilterFavoriteMusicList = favoriteMusicList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_favorite_music_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mFilterFavoriteMusicList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val musicData = mFilterFavoriteMusicList?.get(position)

        if (!musicData?.resultType!!)
        {
            holder.tv_noResultFound?.visibility = View.GONE
            holder.cardViewResultFound?.visibility = View.VISIBLE

            holder.mFavoriteMusicName?.text = musicData?.FavoriteMusicName

            if (musicData?.isSelected!!)
            {
                holder.mFavoriteMusic?.setImageResource(R.mipmap.heart_selected)
            }
            else
            {
                holder.mFavoriteMusic?.setImageResource(R.mipmap.heart)
            }
        }
        else
        {
            holder.tv_noResultFound?.visibility = View.VISIBLE
            holder.cardViewResultFound?.visibility = View.GONE
        }

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (!musicData.resultType)
                {
                    if (musicData.isSelected)
                    {
                        holder.mFavoriteMusic?.setImageResource(R.mipmap.heart)
                        musicData.isSelected = false
                    }
                    else
                    {
                        holder.mFavoriteMusic?.setImageResource(R.mipmap.heart_selected)
                        musicData.isSelected = true
                    }
                    notifyDataSetChanged()
                }
            }
        })
    }

    override fun getFilter(): Filter {
        if (mFilterResult == null) {
            mFilterResult = RecordFilter()
        }
        return mFilterResult!!
    }

    inner class RecordFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charString = constraint.toString()
            if (charString.isEmpty()) {
                mFilterFavoriteMusicList = mFavoriteMusicList
            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                val filteredList = ArrayList<FavoriteMusicModel>()

                for (row in mFavoriteMusicList!!) {
                    if (row.FavoriteMusicName.toLowerCase(Locale.ROOT).contains(
                            charString.toLowerCase(
                                Locale.ROOT
                            )
                        ) ||
                        row.FavoriteMusicName.contains(charString)
                    ) {
                        filteredList.add(row)
                    }
                }
                if (filteredList.size == 0) {
                    val noDataBean = FavoriteMusicModel()
                    noDataBean.FavoriteMusicName = "No result found."
                    noDataBean.resultType = true
                    filteredList.add(noDataBean)
                }
                mFilterFavoriteMusicList = filteredList
            }
            val filterResults = Filter.FilterResults()
            filterResults.values = mFilterFavoriteMusicList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mFilterFavoriteMusicList = results?.values as ArrayList<FavoriteMusicModel>
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mFavoriteMusicName: TextView? = null
        var mFavoriteMusic: ImageView? = null
        var tv_noResultFound: TextView? = null
        var cardViewResultFound: CardView? = null

        init {
            mFavoriteMusicName = itemView.findViewById(R.id.tv_artistName)
            mFavoriteMusic = itemView.findViewById(R.id.iv_favorite)
            tv_noResultFound = itemView.findViewById(R.id.tv_noResultFound)
            cardViewResultFound = itemView.findViewById(R.id.lyt_resultFound)
        }
    }
}