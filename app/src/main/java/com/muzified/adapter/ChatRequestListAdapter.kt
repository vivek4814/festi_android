package com.muzified.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.ChatRequestModel
import java.util.*
import kotlin.collections.ArrayList

class ChatRequestListAdapter(context: Context, requestList: ArrayList<ChatRequestModel>, itemListener: ItemClickListenerWithViewType) : RecyclerView.Adapter<ChatRequestListAdapter.MyViewHolder>(),
    Filterable {

    private var mContext: Context? = null
    private var mChatRequestList: ArrayList<ChatRequestModel>? = null
    private var mFilterChatRequestList: ArrayList<ChatRequestModel>? = null
    private var mClickListener: ItemClickListenerWithViewType? = null
    private var mFilterResult: Filter? = null

    init {
        this.mContext = context
        this.mChatRequestList = requestList
        this.mFilterChatRequestList = requestList
        this.mClickListener = itemListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_chat_request_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mFilterChatRequestList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val requestUserData = mFilterChatRequestList?.get(position)

        if (!requestUserData?.resultType!!)
        {
            holder.tv_noResultFound?.visibility = View.GONE
            holder.cardViewResultFound?.visibility = View.VISIBLE

            if (!TextUtils.isEmpty(requestUserData?.from_image))
            {
                Glide.with(mContext!!)
                    .load(requestUserData?.from_image)
                    .centerCrop()
                    .placeholder(R.drawable.place_holder)
                    .into(holder.mProfileImage!!)
            }

            if (!TextUtils.isEmpty(requestUserData?.first_name))
            {
                holder.mUserName?.text = requestUserData?.first_name + " " + requestUserData?.last_name
            }
            else
            {
                holder.mUserName?.text = ""
            }
        }
        else
        {
            holder.tv_noResultFound?.visibility = View.VISIBLE
            holder.cardViewResultFound?.visibility = View.GONE
        }

        holder.acceptRequestBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (!requestUserData.resultType)
                {
                    mClickListener?.onItemClick(position, holder.acceptRequestBtn!!)
                }
            }
        })

        holder.rejectRequestBtn?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (!requestUserData.resultType)
                {
                    mClickListener?.onItemClick(position, holder.rejectRequestBtn!!)
                }
            }
        })
        holder.parentLayout?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if (!requestUserData.resultType)
                {
                    mClickListener?.onItemClick(position, holder.parentLayout!!)
                }
            }
        })
    }

    override fun getFilter(): Filter {
        if (mFilterResult == null) {
            mFilterResult = RecordFilter()
        }
        return mFilterResult!!
    }

    inner class RecordFilter: Filter()
    {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val charString = constraint.toString()
            if (charString.isEmpty()) {
                mFilterChatRequestList = mChatRequestList
            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                val filteredList = ArrayList<ChatRequestModel>()

                for (row in mChatRequestList!!) {
                    if (row.first_name.toLowerCase(Locale.ROOT).contains(charString.toLowerCase(
                            Locale.ROOT)) ||
                        row.first_name.contains(charString))
                    {
                        filteredList.add(row)
                    }
                }
                if (filteredList.size == 0) {
                    val noDataBean = ChatRequestModel()
                    noDataBean.first_name = "No result found."
                    noDataBean.resultType = true
                    filteredList.add(noDataBean)
                }
                mFilterChatRequestList = filteredList
            }
            val filterResults = Filter.FilterResults()
            filterResults.values = mFilterChatRequestList
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mFilterChatRequestList = results?.values as ArrayList<ChatRequestModel>
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mProfileImage: ImageView? = null
        var mUserName: TextView? = null
        var acceptRequestBtn: ImageView? = null
        var rejectRequestBtn: ImageView? = null
        var parentLayout: ConstraintLayout? = null
        var tv_noResultFound: TextView? = null
        var cardViewResultFound: ConstraintLayout? = null

        init {
            mProfileImage = itemView.findViewById(R.id.iv_profileImage)
            mUserName = itemView.findViewById(R.id.tv_requesterName)
            acceptRequestBtn = itemView.findViewById(R.id.iv_acceptRequest)
            rejectRequestBtn = itemView.findViewById(R.id.iv_rejectRequest)
            parentLayout = itemView.findViewById(R.id.lyt_parent)
            tv_noResultFound = itemView.findViewById(R.id.tv_noResultFound)
            cardViewResultFound = itemView.findViewById(R.id.lyt_resultFound)
        }
    }
}