package com.muzified.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.model.DistanceStepBean

class DistanceStepCountAdapter(
    context: Context,
    distanceList: ArrayList<DistanceStepBean>
) : RecyclerView.Adapter<DistanceStepCountAdapter.MyViewHolder>() {

    private var mContext: Context? = null
    private var mDistanceStepList: ArrayList<DistanceStepBean>? = null

    init {
        this.mContext = context
        this.mDistanceStepList = distanceList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_distance_step_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mDistanceStepList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val stepItem = mDistanceStepList?.get(position)

        holder.mStepLabel?.text = stepItem?.label

        when (stepItem?.status) {
            AppConstants.COMPLETED -> {
                holder.mStepStatus?.setImageResource(R.mipmap.circle_pink_small)
                holder.mStepLine?.setBackgroundColor(mContext?.resources?.getColor(R.color.colorPink)!!)
            }
            AppConstants.CURRENT -> {
                holder.mStepStatus?.setImageResource(R.mipmap.circle_pink_large)
                holder.mStepLine?.setBackgroundColor(mContext?.resources?.getColor(R.color.colorPink)!!)
            }
            AppConstants.NOT_COMPLETED -> {
                holder.mStepStatus?.setImageResource(R.mipmap.circle_white)
                holder.mStepLine?.setBackgroundColor(mContext?.resources?.getColor(R.color.colorWhite)!!)
            }
        }

        if (position <= mDistanceStepList?.size!! - 1) {
            holder.mStepLine?.visibility = View.GONE
        }
        else
        {
            holder.mStepLine?.visibility = View.VISIBLE
        }

//        holder.itemView?.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                holder.mFavoriteArtist?.setImageResource(R.mipmap.heart_selected)
//                //mItemClickListener?.onItemClickListener(v!!, position)
//            }
//        })
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mStepStatus: ImageView? = null
        var mStepLabel: TextView? = null
        var mStepLine: View? = null

        init {
            mStepStatus = itemView.findViewById(R.id.iv_stepStatus)
            mStepLabel = itemView.findViewById(R.id.tv_labelName)
            mStepLine = itemView.findViewById(R.id.view)
        }
    }
}