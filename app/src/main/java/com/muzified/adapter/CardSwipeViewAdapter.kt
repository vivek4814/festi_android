package com.muzified.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.Utils.DateUtils
import com.muzified.Utils.Prefs
import com.muzified.Utils.Utility
import com.muzified.listener.ItemClickListenerWithViewType
import com.muzified.model.SignUpModel

class CardSwipeViewAdapter(
    context: Context,
    items: ArrayList<SignUpModel>,
    listener: ItemClickListenerWithViewType
) : RecyclerView.Adapter<CardSwipeViewAdapter.MyViewHolder>() {

    private var mContext: Context? = null
    private var mOtherUserProfileList: ArrayList<SignUpModel>? = null
    private var mLayoutInflater: LayoutInflater? = null
    private var mClickListener: ItemClickListenerWithViewType? = null

    init {
        mContext = context
        mOtherUserProfileList = items
        mLayoutInflater = LayoutInflater.from(mContext)
        mClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_cardview_swipe_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val userProfile = mOtherUserProfileList?.get(position)

        if (!TextUtils.isEmpty(userProfile?.profile)) {
            Glide.with(mContext!!)
                .load(userProfile?.profile)
                .centerCrop()
                .placeholder(R.drawable.place_holder)
                .into(holder.otherUserProfileImage!!)
        }
        if (!TextUtils.isEmpty(userProfile?.first_name)) {
            holder.otherUserName?.text = userProfile?.first_name + " " + userProfile?.last_name
        } else {
            holder.otherUserName?.text = ""
        }

        holder.otherUserAge?.text = DateUtils.getCalculatedYearFromDate(userProfile?.dob!!)
        val favouriteArtistList = AppConstants.getArtistList()
        val userFavouriteArtistIds = userProfile.favourites_artist_ids
        var favouriteArtistName = ""
        if (!userFavouriteArtistIds.isNullOrEmpty()) {
            if (!favouriteArtistList.isNullOrEmpty()) {
                for (j in 0 until userFavouriteArtistIds.size) {
                    val favouriteArtistId = userFavouriteArtistIds.get(j)
                    for (i in 0 until favouriteArtistList.size) {
                        val artistModel = favouriteArtistList.get(i)
                        if (favouriteArtistId.equals(artistModel.id)) {
                            if (!TextUtils.isEmpty(favouriteArtistName)) {
                                favouriteArtistName =
                                    favouriteArtistName + ", " + artistModel.FavoriteArtistName
                            } else {
                                favouriteArtistName = artistModel.FavoriteArtistName
                            }
                            break
                        }
                    }
                }
            }
        }

        holder.otherUserFavouriteArtist?.text = favouriteArtistName
        if (!TextUtils.isEmpty(userProfile.lat) && !TextUtils.isEmpty(userProfile.long)) {
            val userLat = Prefs.getString(AppConstants.USER_LATITUDE).toDouble()
            val userLong = Prefs.getString(AppConstants.USER_LONGITUDE).toDouble()
            val distance = Utility.calculateDistanceBetweenTwoLatLong(
                userLat,
                userLong,
                userProfile.lat.toDouble(),
                userProfile.long.toDouble()
            )
            val calculatedDistance = distance.toInt()
            holder.otherUserDistance?.text = calculatedDistance.toString() + " mile(s)"
        } else {
            holder.otherUserDistance?.text = ""
        }


        holder.optionImage?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                mClickListener?.onItemClick(position, v!!)
            }
        })
        holder.parentView?.setOnClickListener {
            //val item = getItem(position) as String
            mClickListener?.onItemClick(position, holder.parentView!!)
        }

    }

    override fun getItemCount(): Int {
        return mOtherUserProfileList?.size!!
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var parentView: LinearLayout? = null
        var otherUserProfileImage: ImageView? = null
        var optionImage: ImageView? = null
        var otherUserName: TextView? = null
        var otherUserAge: TextView? = null
        var otherUserFavouriteArtist: TextView? = null
        var otherUserDistance: TextView? = null

        init {
            parentView = itemView.findViewById(R.id.lyt_parent)
            otherUserProfileImage = itemView.findViewById(R.id.imageView)
            optionImage = itemView.findViewById(R.id.iv_option)
            otherUserName = itemView.findViewById(R.id.tv_userName)
            otherUserAge = itemView.findViewById(R.id.tv_userAge)
            otherUserFavouriteArtist = itemView.findViewById(R.id.tv_favoriteArtist)
            otherUserDistance = itemView.findViewById(R.id.tv_distance)
        }
    }

//    override fun getItem(position: Int): Any {
//        return mOtherUserProfileList?.get(position)!!
//    }
//
//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }
//
//    override fun getCount(): Int {
//        return mOtherUserProfileList?.size!!
//    }
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
//        val otherUserItem = mOtherUserProfileList?.get(position)
//        var view = convertView
//        if (view == null) {
//            view = mLayoutInflater?.inflate(
//                R.layout.item_cardview_swipe_list,
//                parent,
//                false
//            )
//        }
//        val parentView = view?.findViewById(R.id.lyt_parent) as ConstraintLayout
//        val otherUserProfileImage = view.findViewById(R.id.imageView) as ImageView
//        val optionImage = view.findViewById(R.id.iv_option) as ImageView
//        val otherUserName = view.findViewById(R.id.tv_userName) as TextView
//        val otherUserAge = view.findViewById(R.id.tv_userAge) as TextView
//        val otherUserFavouriteArtist = view.findViewById(R.id.tv_favoriteArtist) as TextView
//        val otherUserDistance = view.findViewById(R.id.tv_distance) as TextView
//
//        if(!TextUtils.isEmpty(otherUserItem?.profile))
//        {
//            Glide.with(mContext!!)
//                .load(otherUserItem?.profile)
//                .centerCrop()
//                .placeholder(R.drawable.place_holder)
//                .into(otherUserProfileImage)
//        }
//        if (!TextUtils.isEmpty(otherUserItem?.first_name))
//        {
//            otherUserName.text = otherUserItem?.first_name + " " + otherUserItem?.last_name
//        }
//        else
//        {
//            otherUserName.text = ""
//        }
//
//        otherUserAge.text = DateUtils.getCalculatedYearFromDate(otherUserItem?.dob!!)
//        val favouriteArtistList = AppConstants.getArtistList()
//        val userFavouriteArtistIds = otherUserItem.favourites_artist_ids
//        var favouriteArtistName = ""
//        if (!userFavouriteArtistIds.isNullOrEmpty())
//        {
//            if (!favouriteArtistList.isNullOrEmpty())
//            {
//                for (j in 0 until userFavouriteArtistIds.size)
//                {
//                    val favouriteArtistId = userFavouriteArtistIds.get(j)
//                    for (i in 0 until favouriteArtistList.size)
//                    {
//                        val artistModel = favouriteArtistList.get(i)
//                        if (favouriteArtistId.equals(artistModel.id)){
//                            if (!TextUtils.isEmpty(favouriteArtistName))
//                            {
//                                favouriteArtistName = favouriteArtistName + ", " + artistModel.FavoriteArtistName
//                            }
//                            else
//                            {
//                                favouriteArtistName = artistModel.FavoriteArtistName
//                            }
//                            break
//                        }
//                    }
//                }
//            }
//        }
//
//        otherUserFavouriteArtist.text = favouriteArtistName
//        if (!TextUtils.isEmpty(otherUserItem.lat) && !TextUtils.isEmpty(otherUserItem.long))
//        {
//            val userLat = Prefs.getString(AppConstants.USER_LATITUDE).toDouble()
//            val userLong = Prefs.getString(AppConstants.USER_LONGITUDE).toDouble()
//            val distance = Utility.calculateDistanceBetweenTwoLatLong(userLat, userLong, otherUserItem.lat.toDouble(), otherUserItem.long.toDouble())
//            val calculatedDistance = distance.toInt()
//            otherUserDistance.text = calculatedDistance.toString() +  " mile(s)"
//        }
//        else
//        {
//            otherUserDistance.text = ""
//        }
//
//
////        optionImage.setOnClickListener(object : View.OnClickListener{
////            override fun onClick(v: View?) {
////                mClickListener?.onItemClick(position, v!!)
////            }
////        })
////        parentView.setOnClickListener {
////            //val item = getItem(position) as String
////            mClickListener?.onItemClick(position, parentView!!)
////        }
//        return view
//    }
}