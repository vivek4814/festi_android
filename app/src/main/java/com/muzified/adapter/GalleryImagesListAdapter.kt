package com.muzified.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.muzified.R
import com.muzified.listener.ItemClickListener
import com.muzified.model.FavoriteArtistModel

class GalleryImagesListAdapter(context: Context, galleryList: ArrayList<String>?) : RecyclerView.Adapter<GalleryImagesListAdapter.MyViewHolder>() {

    private var mContext: Context? = null
    private var mGalleryImageList: ArrayList<String>? = null

    init {
        this.mContext = context
        this.mGalleryImageList = galleryList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_gallery_images, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mGalleryImageList?.size!!
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val galleryListData = mGalleryImageList?.get(position)

        Glide.with(mContext!!)
            .load(galleryListData)
            .centerCrop()
            .placeholder(R.drawable.place_holder)
            .into(holder.mGalleryImage!!)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mGalleryImage: ImageView? = null

        init {
            mGalleryImage = itemView.findViewById(R.id.iv_galleryImage)
        }
    }
}