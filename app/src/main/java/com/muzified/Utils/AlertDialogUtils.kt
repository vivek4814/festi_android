package com.muzified.Utils

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.muzified.activity.LoginActivity
import com.muzified.activity.WelcomeActivity


class AlertDialogUtils {

    companion object
    {
        fun showAlertDialog(from: Activity, title: String, msg: String, positiveBtnText: String, negativeBtnText: String, dialogType: String)
        {
            AlertDialog.Builder(from)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(positiveBtnText, { dialog, _ -> exitFromApp(from, dialog, dialogType) })
                .setNegativeButton(negativeBtnText, { dialog, _ -> dialog.dismiss() })
                .show()
        }

        private fun exitFromApp(from: Activity, dialog: DialogInterface, dialogType: String) {
            dialog.dismiss()
            when(dialogType)
            {
                AppConstants.EXIT_TYPE ->
                {
                    val logout_intent = Intent(from, WelcomeActivity::class.java)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    from.startActivity(logout_intent)
                    from.finish()
                }
                AppConstants.LOGOUT_TYPE ->
                {
                    FirebaseAuth.getInstance().signOut()
                    LoginManager.getInstance().logOut()
                    Prefs.clear()
                    val logout_intent = Intent(from, LoginActivity::class.java)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    from.startActivity(logout_intent)
                    from.finish()
                }
                AppConstants.CLOSE_ACCOUNT_TYPE ->
                {
                    val logout_intent = Intent(from, WelcomeActivity::class.java)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    logout_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    from.startActivity(logout_intent)
                    from.finish()
                }
            }
        }
    }
}