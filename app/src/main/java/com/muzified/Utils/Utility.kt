package com.muzified.Utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*

class Utility {
    companion object
    {
        /**
         * Extension method to show a keyboard for View.
         */
        fun showKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            view.requestFocus()
            imm.showSoftInput(view, 0)
        }

        /**
         * Try to hide the keyboard and returns whether it worked
         * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
         */
        fun hideKeyboard(context: Context, view: View) {
            view.apply {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        //https://stackoverflow.com/questions/6981916/how-to-calculate-distance-between-two-locations-using-their-longitude-and-latitu

        //https://stackoverflow.com/questions/22577075/calculating-the-distance-between-two-latitude-and-longitude-points-in-android
        fun calculateDistanceBetweenTwoLatLong(
            lat1: Double,
            lon1: Double,
            lat2: Double,
            lon2: Double
        ): Double {

            val locationA = Location("point A")
            locationA.setLatitude(lat1)
            locationA.setLongitude(lon1)
            val locationB = Location("point B")
            locationB.setLatitude(lat2)
            locationB.setLongitude(lon2)

            val distance = locationA.distanceTo(locationB)
            val calculatedDistance = distance * 0.000621371
            return calculatedDistance
//            val theta = lon1 - lon2
//            var dist = (Math.sin(deg2rad(lat1))
//                    * Math.sin(deg2rad(lat2))
//                    + (Math.cos(deg2rad(lat1))
//                    * Math.cos(deg2rad(lat2))
//                    * Math.cos(deg2rad(theta))))
//            dist = Math.acos(dist)
//            dist = rad2deg(dist)
//            dist = dist * 60 * 1.1515
//            return dist
        }

        private fun deg2rad(deg: Double): Double {
            return deg * Math.PI / 180.0
        }

        private fun rad2deg(rad: Double): Double {
            return rad * 180.0 / Math.PI
        }

        fun getCompleteAddressFromLatLong(context: Context, latitude: Double, longitude: Double) : String
        {
            var address: String = ""
            if (longitude != null)
            {
                val geocoder: Geocoder
                val addresses: List<Address>
                geocoder = Geocoder(context, Locale.getDefault())

                try {
                    addresses = geocoder.getFromLocation(
                        latitude,
                        longitude,
                        1
                    ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                    address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                    val city: String = addresses[0].getLocality()
                    val state: String = addresses[0].getAdminArea()
                    val country: String = addresses[0].getCountryName()
                    val postalCode: String = addresses[0].getPostalCode()
                    val knownName: String = addresses[0].getFeatureName()
                }
                catch (e: Exception)
                {
                    Log.e("GetAddressFromLatLong", e.message!!)
                    return address
                }
            }
            return address
        }
    }
}