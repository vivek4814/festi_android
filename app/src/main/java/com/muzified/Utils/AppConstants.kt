package com.muzified.Utils

import com.muzified.R
import com.muzified.model.FavoriteArtistModel
import com.muzified.model.FavoriteMusicModel

class AppConstants {

    companion object
    {
        const val MALE = "MALE"
        const val FEMALE = "FEMALE"
        const val BOTH = "BOTH"

        const val USER_PROFILE = "USER_PROFILE"
        const val CHAT_LIST = "CHAT_LIST"
        const val CHAT_REQUEST = "CHAT_REQUEST"
        const val SWIPING_PREFERENCES = "SWIPING_PREFERENCES"
        const val RATE_US = "RATE_US"
        const val PRIVACY_POLICY = "PRIVACY_POLICY"
        const val TERMS_OF_SERVICES = "TERMS_OF_SERVICES"
        const val TERMS_OF_SERVICE = "BOTH"
        const val CONTACT_US = "CONTACT_US"

        const val COMPLETED = "COMPLETED"
        const val CURRENT = "CURRENT"
        const val NOT_COMPLETED = "NOT_COMPLETED"

        const val EXIT_TYPE = "EXIT_TYPE"
        const val LOGOUT_TYPE = "LOGOUT_TYPE"
        const val CLOSE_ACCOUNT_TYPE = "CLOSE_ACCOUNT_TYPE"
        const val LOGIN_TYPE = "LOGIN_TYPE"

        const val EMAIL_TYPE = "EMAIL_TYPE"
        const val FACEBOOK_TYPE = "FACEBOOK_TYPE"

        const val CAMERA_FOR_IMAGE = 6
        const val CAMERA_FOR_VIDEO = 7
        const val DEFAULT_IMAGE = 0
        const val GALLERY_IMAGE_1 = 1
        const val GALLERY_IMAGE_2 = 2
        const val GALLERY_IMAGE_3 = 3
        const val GALLERY_IMAGE_4 = 4
        const val GALLERY_IMAGE_5 = 5

        const val USER_ID = "USER_ID"
        const val GENDER_PREFERENCE = "GENDER_PREFERENCE"
        const val SELECTED_CONVERSATION_ID = "SELECTED_CONVERSATION_ID"
        const val SELECTED_CONVERSATION_MSG = "SELECTED_CONVERSATION_MSG"
        const val SELECTED_CONVERSATION_USER_DATA = "SELECTED_CONVERSATION_USER_DATA"

        const val USER_LATITUDE = "USER_LATITUDE"
        const val USER_LONGITUDE = "USER_LONGITUDE"

        const val SIGN_UP_USER_DATA = "SIGN_UP_USER_DATA"
        const val SIGN_UP_SCREEN = "SIGN_UP_SCREEN"
        const val PROFILE_PICK_SELECTION_SCREEN = "PROFILE_PICK_SELECTION_SCREEN"
        const val BIO_SEEKING_SCREEN = "BIO_SEEKING_SCREEN"
        const val GALLERY_IMAGE_SCREEN = "GALLERY_IMAGE_SCREEN"

        val MSG_TYPE_IMAGE = 1
        val MSG_TYPE_VIDEO = 3
        val MSG_TYPE_TEXT = 0

        val FILE_TYPE_IMAGE = 101
        val FILE_TYPE_VIDEO = 102

        val artistName = arrayOf(
            "Bon Jovi",
            "Britney Spears",
            "Bob Dylan",
            "Tupac Shakur",
            "Backstreet Boys",
            "Tim McGraw",
            "Rod Stewart",
            "Taylor Swift",
            "Santana",
            "Alan Jackson",
            "Guns N’ Roses",
            "Eminem",
            "Kenny Rogers",
            "Shania Twain",
            "Kenny G",
            "Journey",
            "Celine Dion"
        )
        val artistImage = arrayOf(
            R.drawable.a_1,
            R.drawable.a_2,
            R.drawable.a_3,
            R.drawable.a_4,
            R.drawable.a_5,
            R.drawable.a_6,
            R.drawable.a_7,
            R.drawable.a_8,
            R.drawable.a_9,
            R.drawable.a_10,
            R.drawable.a_11,
            R.drawable.a_12,
            R.drawable.a_13,
            R.drawable.a_14,
            R.drawable.a_15,
            R.drawable.a_16,
            R.drawable.a_17
        )

        fun getArtistList(): ArrayList<FavoriteArtistModel>
        {
            val artistList = ArrayList<FavoriteArtistModel>()
            for (i in 0 until artistName.size - 1) {
                val artist = FavoriteArtistModel()
                artist.id = i.toString()
                artist.FavoriteArtistName = artistName[i]
                artist.Image = artistImage[i]
                artist.isSelected = false
                artistList.add(artist)
            }
            return artistList
        }

        val musicName = arrayOf(
            "Rock", "Alternative", "Classic Rock", "Classical", "Jazz", "Blues", "Classical Rock/Progressive", "Oldies", "Progressive Rock", "Progressive Rock", "Rap/hip-hop", "Dance/Electronica", "Folk", "Indie Rock", "Soul/funk", "Soundtracks/theme Songs", "Thrash Metal", "Grunge", "Country", "Muzziac added Púnk Rock", "Axelanti added Reggae"
        )

        fun getFavouriteMusicList(): ArrayList<FavoriteMusicModel>
        {
            val favoriteMusicArrayList = ArrayList<FavoriteMusicModel>()
            for (i in 0 until musicName.size - 1) {
                val artist = FavoriteMusicModel()
                artist.id = i.toString()
                artist.FavoriteMusicName = musicName[i]
                artist.isSelected = false
                favoriteMusicArrayList.add(artist)
            }
            return favoriteMusicArrayList
        }
    }

    enum class Status
    {
        COMPLETED,
        CURRENT,
        NOT_COMPLETED
    }
}