package com.muzified.Utils

import android.text.TextUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class DateUtils {
    companion object
    {
        fun getDate(milliSeconds: Long, dateFormat: String?): String? {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar: Calendar = Calendar.getInstance()
            calendar.setTimeInMillis(milliSeconds)
            return formatter.format(calendar.getTime())
        }

        fun getFormatDOB(date: Date, dateFormat: String?): String? {
            // Create a DateFormatter object for displaying date in specified format.
            val returnDateFormat = SimpleDateFormat(dateFormat)
            val calendar: Calendar = Calendar.getInstance()
            calendar.time = date
            return returnDateFormat.format(calendar.getTime())
        }

        fun getCalculatedYearFromDate(date: Long): String? {
            val dobCalendar = Calendar.getInstance()
            val todayCalendar = Calendar.getInstance()
            dobCalendar.timeInMillis = date
            val calculatedAge = todayCalendar.get(Calendar.YEAR) - dobCalendar.get(Calendar.YEAR)
            return calculatedAge.toString()
        }

        fun convertStringToDate(dtStart: String): Long
        {
            //10/27/2021
            val format = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
            try {
                val date = format.parse(dtStart)
                System.out.println(date)
                return date.time
            } catch (e: ParseException) {
                e.printStackTrace()
                return 0
            }
        }

        fun getDateAndTime(milliSeconds: Long, dateFormat: String?): String? {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }
    }
}