package com.muzified.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.muzified.R
import com.muzified.Utils.AppConstants
import com.muzified.activity.BioSeekingActivity
import com.muzified.activity.GalleryPhotoActivity
import com.muzified.activity.ProfilePickSelectionActivity
import com.muzified.activity.SignUpActivity
import com.muzified.model.SignUpModel


class DialogUtils {
    companion object {
        fun showAlertDetailDialog(
            activity: Activity,
            signUpModel: SignUpModel,
            type: String
        ) {
            val dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setContentView(R.layout.dialog_profile_incomplete)

            val detailMsg = dialog.findViewById(R.id.tv_detailMsg) as TextView
            val btnOk = dialog.findViewById(R.id.tv_ok) as TextView
            btnOk.setOnClickListener {
                dialog.dismiss()
                when (type) {
                    AppConstants.SIGN_UP_SCREEN -> {
                        val signUpIntent = Intent(activity, SignUpActivity::class.java)
                        signUpIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        signUpIntent.putExtra(AppConstants.SIGN_UP_USER_DATA, signUpModel)
                        activity.startActivity(signUpIntent)
                    }
                    AppConstants.PROFILE_PICK_SELECTION_SCREEN -> {
                        val profilePickSelectionIntent = Intent(
                            activity,
                            ProfilePickSelectionActivity::class.java
                        )
                        profilePickSelectionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        profilePickSelectionIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        profilePickSelectionIntent.putExtra(
                            AppConstants.SIGN_UP_USER_DATA,
                            signUpModel
                        )
                        activity.startActivity(profilePickSelectionIntent)
                    }
                    AppConstants.BIO_SEEKING_SCREEN -> {
                        val bioSeekingIntent = Intent(activity, BioSeekingActivity::class.java)
                        bioSeekingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        bioSeekingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        activity.startActivity(bioSeekingIntent)
                    }
                    AppConstants.GALLERY_IMAGE_SCREEN -> {
                        val galleryImageSelectionIntent = Intent(
                            activity,
                            GalleryPhotoActivity::class.java
                        )
                        galleryImageSelectionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        galleryImageSelectionIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        activity.startActivity(galleryImageSelectionIntent)
                    }
                }
                activity.finish()
                activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
            dialog.show()
            dialog.getWindow()?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        }

        fun openAlertDialog(context: Context, message: String) {
            val alertDialog = AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .show()
        }
    }
}